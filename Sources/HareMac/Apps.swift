//
// Created by entaoyang@163.com on 2022/9/13.
//

import AppKit
import Foundation
import HareSwift
import SwiftUI

var appDelegate: (NSObject & NSApplicationDelegate)? = nil

@discardableResult
public func HareMain<T: NSObject & NSApplicationDelegate>(delegate: T.Type) -> Int32 {
    let app = NSApplication.shared
    let delegate = T.init()
    app.delegate = delegate
    appDelegate = delegate
    return NSApplicationMain(CommandLine.argc, CommandLine.unsafeArgv)
}

open class HareAppDelegate: NSObject, NSApplicationDelegate {

    @objc
    open func applicationDidFinishLaunching(_ aNotification: Notification) {
    }

    @objc
    open func applicationWillTerminate(_ aNotification: Notification) {
        AppExitClean.shared.clean()
    }

    @objc
    open func applicationSupportsSecureRestorableState(_ app: NSApplication) -> Bool {
        true
    }

    @objc
    open func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
        true
    }
}

extension NSWindow {

    public func maybeClose() {
        self.performClose(self)
    }

    public func closeMe() {
        self.performClose(self)
    }
}

open class HareWindow: NSWindow {
    var closeCallback: (() -> Bool)? = nil

    public init(
        page: NSViewController?, title: String? = nil, rect: NSRect = .init(width: 900, height: 500), style: NSWindow.StyleMask = [.titled, .closable, .miniaturizable, .resizable]
    ) {
        super.init(contentRect: rect, styleMask: style, backing: .buffered, defer: false)
        self.isReleasedWhenClosed = false
        self.toolbarStyle = .unified
        self.contentViewController = page
        self.title = title ?? page?.title ?? NSApp.appName
        self.center()
        if let h = page as? HarePage {
            self.delegate = h
            h.onConfig(window: self)
        }
    }

    //NSHostingView(rootView: view)
    public init(
        view: NSView?, title: String? = nil, rect: NSRect = .init(width: 900, height: 500),
        style: NSWindow.StyleMask = [.titled, .closable, .miniaturizable, .resizable, .fullSizeContentView]
    ) {
        super.init(contentRect: rect, styleMask: style, backing: .buffered, defer: false)
        self.isReleasedWhenClosed = false
        self.toolbarStyle = .unified
        self.contentView = view
        self.title = title ?? NSApp.appName
        self.center()
    }

    open override func close() {
        if let c = closeCallback {
            if !c() {
                return
            }
        }
        if self === NSApp.modalWindow {
            NSApp.stopModal()
        }
        super.close()
    }

}

extension NSApplication {

    @discardableResult
    public func show<T: NSView>(view: T, title: String? = nil, level: NSWindow.Level? = nil) -> T {
        let window = HareWindow(view: view, title: title)
        if let lv = level {
            window.level = lv
        }
        window.makeKeyAndOrderFront(nil)
        return view
    }

    @discardableResult
    public func show<T: NSViewController>(modal page: T, title: String? = nil, level: NSWindow.Level? = nil) -> T {
        let window = HareWindow(page: page, title: title)
        if let lv = level {
            window.level = lv
        }
        self.runModal(for: window)
        return page
    }

    @discardableResult
    public func show<T: NSViewController>(page: T, title: String? = nil, level: NSWindow.Level? = nil) -> T {
        let window = HareWindow(page: page, title: title)
        if let lv = level {
            window.level = lv
        }
        window.makeKeyAndOrderFront(nil)
        return page
    }

    @discardableResult
    public func show<P: NSViewController>(singlton type: P.Type, title: String? = nil, level: NSWindow.Level? = nil, _ block: (P) -> Void = { _ in }) -> P {
        let wid: Ident = Ident(type.className())
        for w in self.windows {
            if w.identifier == wid {
                w.orderFront(nil)
                return w.contentViewController as! P
            }
        }
        let page = P.init()
        block(page)
        let window = HareWindow(page: page, title: title)
        window.identifier = wid
        if let lv = level {
            window.level = lv
        }
        window.makeKeyAndOrderFront(nil)
        return page
    }

}

extension Bundle {
    public func info(_ key: String) -> Any? {
        self.object(forInfoDictionaryKey: key)
    }

    public var displayName: String {
        if let v = self.info("CFBundleDisplayName") {
            return "\(v)"
        }
        return ""
    }
    public var buldleName: String {
        if let v = self.info("CFBundleName") {
            return "\(v)"
        }
        return ""
    }
    public var versionCode: Int {
        if let v = self.info("CFBundleVersion") {
            return Int("\(v)") ?? 0
        }
        return 0
    }

    public var versionName: String {
        if let v = self.info("CFBundleShortVersionString") {
            return "\(v)"
        }
        return ""
    }

}
