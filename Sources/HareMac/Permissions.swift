//
// Created by entaoyang@163.com on 2022/9/22.
//

import AppKit
import Foundation
import HareSwift

private let KEY_PERM = "perm_list"
private let urlPermDB: UserDefaults = UserDefaults.standard

public class URLAccess {
    private let url: URL
    private var bookmark: Bool
    init(url: URL, bookmark: Bool) {
        self.url = url
        self.bookmark = bookmark

    }
    deinit {
        if bookmark {
            url.stopAccessingSecurityScopedResource()
        }
    }
    public func release() {
        if bookmark {
            bookmark = false
            url.stopAccessingSecurityScopedResource()
        }
    }
}

extension URL {
    public func startWriting() async -> URLAccess? {
        if self.fileWritable {
            return URLAccess(url: self, bookmark: false)
        }
        //检查之前获取到的权限
        if let url = self.bookmarkSearch() {
            if url.startAccessingSecurityScopedResource() {
                return URLAccess(url: url, bookmark: true)
            }
        }
        if !self.fileWritable {
            await self.requireAuthorization()
        }
        if self.fileWritable {
            return URLAccess(url: self, bookmark: false)
        }
        return nil
    }
    public func startReading() async -> URLAccess? {
        if self.fileReadable {
            return URLAccess(url: self, bookmark: false)
        }
        //检查之前获取到的权限
        if let url = self.bookmarkSearch() {
            if url.startAccessingSecurityScopedResource() {
                return URLAccess(url: url, bookmark: true)
            }
        }
        if !self.fileReadable {
            await self.requireAuthorization()
        }
        if self.fileReadable {
            return URLAccess(url: self, bookmark: false)
        }
        return nil
    }
    @discardableResult
    public func allowWrite(_ block: () async throws -> Void) async -> Bool {
        //本身就有权限
        if self.fileWritable {
            try? await block()
            return true
        }
        //检查之前获取到的权限
        if let url = self.bookmarkSearch() {
            if url.startAccessingSecurityScopedResource() {
                try? await block()
                url.stopAccessingSecurityScopedResource()
                return true
            }
        }
        if !self.fileWritable {
            await self.requireAuthorization()
        }
        if self.fileWritable {
            try? await block()
            return true
        }
        return false
    }
    @discardableResult
    public func allowRead(_ block: () async throws -> Void) async -> Bool {
        //本身就有权限
        if self.fileReadable {
            try? await block()
            return true
        }
        //检查之前获取到的权限
        if let url = self.bookmarkSearch() {
            if url.startAccessingSecurityScopedResource() {
                try? await block()
                url.stopAccessingSecurityScopedResource()
                return true
            }
        }
        if !self.fileReadable {
            await self.requireAuthorization()
        }
        if self.fileReadable {
            try? await block()
            return true
        }
        return false
    }
    @MainActor
    @discardableResult
    public func requireAuthorization() async -> URL? {
        return await authorizeDirectory(url: self)
    }

}

extension URL {
    //返回的url， 不一定是参数提供的url所during的， 也可能是它的上级目录
    public func bookmarkSearch() -> URL? {
        guard let map: [String: Any] = urlPermDB.dictionary(forKey: KEY_PERM) else {
            return nil
        }
        let path = self.path
        if let data: Data = map[path] as? Data, let u: URL = bookmarkValid(data: data) {
            return u
        }
        for (k, v) in map {
            if path.hasPrefix(k) {
                if let data: Data = v as? Data, let u: URL = bookmarkValid(data: data) {
                    return u
                }
            }
        }
        return nil
    }

    @discardableResult
    public func bookmarkSave() -> Bool {
        if let data: Data = try? self.bookmarkData(options: .withSecurityScope) {
            var map: [String: Any] = urlPermDB.dictionary(forKey: KEY_PERM) ?? [:]
            map[self.path] = data
            urlPermDB.setValue(map, forKey: KEY_PERM)
            urlPermDB.synchronize()
            return true
        }
        return false
    }

    private func bookmarkValid(data: Data) -> URL? {
        var isStale = false
        if let u = try? URL(resolvingBookmarkData: data, options: .withSecurityScope, relativeTo: nil, bookmarkDataIsStale: &isStale) {
            if !isStale {
                return u
            }
        }
        return nil
    }

}

public func access<R>(writing: URL? = nil, reading: URL? = nil, _ block: @escaping () async throws -> R) async throws -> R {
    var wAcc: URLAccess? = nil
    if let wUrl = writing {
        guard let acc = await wUrl.startWriting() else {
            throw HareError("没有写权限")
        }
        wAcc = acc
    }
    defer {
        wAcc?.release()
    }
    var rAcc: URLAccess? = nil
    if let rUrl = reading {
        guard let acc = await rUrl.startWriting() else {
            throw HareError("没有读权限")
        }
        rAcc = acc
    }
    defer {
        rAcc?.release()
    }
    return try await block()
}
