//
//  BaseLayoutView.swift
//  HareMac
//
//  Created by Entao on 2025/1/2.
//

import AppKit
import Foundation
import HareSwift

open class BaseLayoutView: XView {

    public var paddings: EdgeInsets = .zero {
        didSet {
            updateSafeGuide()
        }
    }

    let contentGuide: XLayoutGuide = XLayoutGuide()
    private var leftC: NSLayoutConstraint? = nil
    private var widthC: NSLayoutConstraint? = nil
    private var topC: NSLayoutConstraint? = nil
    private var heightC: NSLayoutConstraint? = nil

    var itemContraints: [NSLayoutConstraint] = []

    private var needBuildConstraints = false

    #if os(macOS)
        public var backgroundColor: NSColor? = nil {
            didSet {
                needsDisplay = true
            }
        }
        public var borderColor: NSColor? = nil {
            didSet {
                needsDisplay = true
            }
        }
        public var borderWidth: CGFloat = 0 {
            didSet {
                needsDisplay = true
            }
        }
        public var cornerRadius: CGFloat = 0 {
            didSet {
                needsDisplay = true
            }
        }

        public func styleBox() {
            self.backgroundColor = .windowBackgroundColor
            self.borderColor = .placeholderTextColor
            self.borderWidth = 1
            self.cornerRadius = 8
        }

        open override func draw(_ dirtyRect: NSRect) {
            let rect = self.bounds

            if let bc = backgroundColor {
                let path = NSBezierPath(roundedRect: rect, xRadius: cornerRadius, yRadius: cornerRadius)
                bc.setFill()
                path.fill()
            }
            if let sc = borderColor, borderWidth > 0 {
                let x = borderWidth / 2
                let path = NSBezierPath(roundedRect: rect.inset(by: .all(x)), xRadius: cornerRadius - x, yRadius: cornerRadius - x)
                path.lineWidth = borderWidth
                sc.set()
                path.stroke()
            }

            super.draw(dirtyRect)
        }
    #endif

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addLayoutGuide(contentGuide)
        leftC = contentGuide.leftAnchor.constraint(equalTo: self.leftAnchor, constant: paddings.left).active()
        topC = contentGuide.topAnchor.constraint(equalTo: self.topAnchor, constant: paddings.top).active()
        widthC = contentGuide.widthAnchor.constraint(equalTo: self.widthAnchor, constant: -paddings.hor).priority(.dragThatCanResizeWindow).active()
        heightC = contentGuide.heightAnchor.constraint(equalTo: self.heightAnchor, constant: -paddings.ver).priority(.dragThatCanResizeWindow).active()

        onInit()
    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    open func onInit() {

    }
    open func onBuildConstraints() -> [NSLayoutConstraint] {
        return []
    }
    private func buildConstraints() {
        for c in itemContraints {
            c.isActive = false
        }
        itemContraints.removeAll()
        let ls = onBuildConstraints()
        for item in ls {
            item.isActive = true
        }
        itemContraints = ls
    }
    public func needsRebuild() {
        self.needBuildConstraints = true
        setNeedsLayout()
    }

    private func updateSafeGuide() {
        leftC?.constant = paddings.left
        widthC?.constant = -paddings.hor
        topC?.constant = paddings.top
        heightC?.constant = -paddings.ver
        setNeedsLayout()
    }

    #if os(iOS)
        open override func layoutSubviews() {
            if needBuildConstraints && self.frame != .zero {
                needBuildConstraints = false
                self.buildConstraints()
            }
            super.layoutSubviews()
        }
    #else
        open override func layout() {
            if needBuildConstraints && self.frame != .zero {
                needBuildConstraints = false
                self.buildConstraints()
            }
            super.layout()
        }
    #endif

    open override func didAddSubview(_ subview: XView) {
        subview.translatesAutoresizingMaskIntoConstraints = false
        needsRebuild()
        super.didAddSubview(subview)
    }

    open override func willRemoveSubview(_ subview: XView) {
        needsRebuild()
        super.willRemoveSubview(subview)
    }
}
