//
// Created by entaoyang@163.com on 2022/9/13.
//

import AppKit
import Foundation
import HareSwift

extension NSRect {

    public init(width: CGFloat, height: CGFloat) {
        self.init(x: 0, y: 0, width: width, height: height)
    }
}
public func lineHeight(font: NSFont) -> CGFloat {
    return NSString("你好").size(withAttributes: [.font: font]).height
}

extension NSTextField {
    public var lineHeightByFont: CGFloat {
        let font = self.font ?? NSFont.labelFont(ofSize: NSFont.labelFontSize)
        return lineHeight(font: font)
    }
}

extension NSView {

    public var backColor: NSColor? {
        get {
            guard let color = self.layer?.backgroundColor else {
                return nil
            }
            return NSColor(cgColor: color)
        }
        set {
            self.wantsLayer = true
            self.layer?.backgroundColor = newValue?.cgColor
            self.layer?.setNeedsDisplay()
        }
    }

    public func shadow() {
        self.wantsLayer = true
        self.layer?.cornerRadius = 8
        self.layer?.masksToBounds = true
        self.shadow = NSShadow()
        self.layer?.shadowOffset = NSSize(width: 3, height: -3)
        self.layer?.shadowColor = NSColor.shadowColor.cgColor  //NSColor.black.cgColor
        self.layer?.shadowOpacity = 0.6
        self.layer?.shadowRadius = 3
    }

    public var isVisible: Bool {
        get {
            !self.isHidden
        }
        set {
            self.isHidden = !newValue
        }
    }
    public func roundBorder(radius: CGFloat? = nil, border: CGFloat? = nil, color: NSColor? = nil, fill: NSColor? = nil) {
        self.layerBorder(radius: radius, border: border, color: color, fill: fill)
    }
    public func layerBorder(radius: CGFloat? = nil, border: CGFloat? = nil, color: NSColor? = nil, fill: NSColor? = nil) {
        self.wantsLayer = true
        self.layer?.masksToBounds = true
        if let r = radius, r >= 0 {
            self.layer?.cornerRadius = r
        }
        if let c = color {
            self.layer?.borderColor = c.cgColor
        }
        if let w = border, w >= 0 {
            self.layer?.borderWidth = w
        }
        if let c = fill {
            self.layer?.backgroundColor = c.cgColor
        }

    }

    public func anchorVerticalClose(view: NSView? = nil, bottom: CGFloat = 0) {
        self.anchors { a in
            if let lastView = view ?? self.subviews.last {
                a.bottom.equal(view: lastView, constant: bottom)
            }
        }
    }

    @discardableResult
    public func anchorHorizontalNext(
        left: CGFloat = 0, right: CGFloat? = nil, top: CGFloat = 0, bottom: CGFloat? = nil,
        centerX: Bool = false, centerY: Bool = false,
        minWidth: CGFloat = 0, width: CGFloat? = nil, fillX: Bool = false,
        minHeight: CGFloat = 0, height: CGFloat? = nil, fillY: Bool = false
    ) -> NSView {
        self.anchors {
            if centerX {
                $0.centerX()
            } else {
                $0.toRightPre(left)
            }
            if let r = right {
                $0.right(r)
            }

            let heightGiven = bottom != nil
            if centerY {
                $0.centerY()
            } else {
                $0.top(top)
                if let b = bottom {
                    $0.bottom(b)
                    if minHeight > 0 {
                        $0.height.ge(constant: minHeight)
                    }
                }
            }
            if !heightGiven {
                if let h = height, h > 0 {
                    $0.height(h)
                } else if fillY {
                    $0.stretchContent(.vertical)
                    if minHeight > 0 {
                        $0.height.ge(constant: minHeight)
                    }
                } else {
                    if minHeight > 0 {
                        $0.height.ge(constant: minHeight)
                    }
                    $0.keepContent(.vertical)
                }
            }

            if let w = width, w > 0 {
                $0.width(w)
            } else if fillX {
                $0.stretchContent(.horizontal)
                if minWidth > 0 {
                    $0.width.ge(constant: minWidth)
                }
            } else {
                if minWidth > 0 {
                    $0.width.ge(constant: minWidth)
                }
                $0.keepContent(.horizontal)
            }
        }
        return self
    }
    @discardableResult
    public func anchorVerticalNext(
        left: CGFloat = 0, right: CGFloat? = nil, edgeX: CGFloat? = nil,
        top: CGFloat = 0, bottom: CGFloat? = nil,
        centerX: Bool = false, centerY: Bool = false,
        minWidth: CGFloat = 0, width: CGFloat? = nil, fillX: Bool = false,
        minHeight: CGFloat = 0, height: CGFloat? = nil, fillY: Bool = false
    ) -> NSView {
        self.anchors {
            let widthGiven = right != nil
            if centerX {
                $0.centerX()
            } else if let ex = edgeX {
                $0.left(ex)
                $0.right(-ex)
                if minWidth > 0 {
                    $0.width.ge(constant: minWidth)
                }
            } else {
                $0.left(left)
                if let r = right {
                    $0.right(r)
                    if minWidth > 0 {
                        $0.width.ge(constant: minWidth)
                    }
                }
            }

            if centerY {
                $0.centerY()
            } else {
                $0.belowPre(top)
            }
            if let b = bottom {
                $0.bottom(b)
            }

            if let h = height, h > 0 {
                $0.height(h)
            } else if fillY {
                $0.stretchContent(.vertical)
                if minHeight > 0 {
                    $0.height.ge(constant: minHeight)
                }
            } else {
                if minHeight > 0 {
                    $0.height.ge(constant: minHeight)
                }
                $0.keepContent(.vertical)
            }

            if !widthGiven {
                if let w = width, w > 0 {
                    $0.width(w)
                } else if fillX {
                    $0.stretchContent(.horizontal)
                    if minWidth > 0 {
                        $0.width.ge(constant: minWidth)
                    }
                } else {
                    if minWidth > 0 {
                        $0.width.ge(constant: minWidth)
                    }
                    $0.keepContent(.horizontal)
                }
            }
        }
        return self
    }

    public func lineWrap(left: CGFloat = 16, right: CGFloat = -16, top: CGFloat = 12, minHeight: CGFloat = 20) -> NSView {
        return self.view { v in
            v.anchors {
                $0.left(left)
                $0.right(right)
                $0.belowPre(top)
                $0.height.ge(constant: minHeight)
                $0.keepContent(.vertical)
            }
        }
    }
}
