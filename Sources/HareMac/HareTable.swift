//
// Created by entaoyang@163.com on 2022-11-26.
//

import AppKit
import Foundation
import HareSwift

public struct TableIndex {
    public let row: Int
    public let col: Int
}

public class TableContextMenuItem {
    public let title: String
    public let ident: Ident
    public var onValidate: ((Set<Int>) -> Bool)? = nil
    public var onAction: ((Set<Int>) -> Void)? = nil
    public var menuItem: NSMenuItem? = nil

    init(title: String, ident: Ident? = nil) {
        self.title = title
        self.ident = ident ?? Ident(title)
    }
}

open class HareTable<M: Any>: NSScrollView, NSTableViewDataSource, NSTableViewDelegate, NSMenuDelegate {
    public let tableView = NSTableView(frame: .zero)
    public var items: [M] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }

    private var colList: [HareTableColumn<M>] = []
    private var doubleClickCallback: ((TableIndex) -> Void)? = nil
    private var contextMenuItems: [TableContextMenuItem] = []
    private var heightMap: [Int: CGFloat] = [:]

    var onOrderChanged: VoidBlock? = nil

    public lazy var defaultLabelCreator: () -> NSView = { [weak self] in
        let a = Label(frame: .zero)
        a.alignment = .left
        a.alignVertical = .center
        a.font = .systemFont(ofSize: 15)
        a.maximumNumberOfLines = 10
        return a
    }

    public override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)

        self.hasVerticalScroller = true
        self.autohidesScrollers = true
        self.documentView = self.tableView

        self.tableView.gridStyleMask = .dashedHorizontalGridLineMask
        self.tableView.rowSizeStyle = .custom
        self.tableView.rowHeight = 36

        self.tableView.dataSource = self
        self.tableView.delegate = self
        onInit()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    open func onInit() {

    }
    public func allowOrder(orderChanged: @escaping VoidBlock) {
        self.onOrderChanged = orderChanged
        tableView.setDraggingSourceOperationMask(.move, forLocal: true)
        tableView.registerForDraggedTypes([.string])
    }
    open func tableView(_ tableView: NSTableView, pasteboardWriterForRow row: Int) -> (any NSPasteboardWriting)? {
        return row.toString() as NSString
    }
    open func tableView(_ tableView: NSTableView, validateDrop info: any NSDraggingInfo, proposedRow row: Int, proposedDropOperation dropOperation: NSTableView.DropOperation)
        -> NSDragOperation
    {
        if dropOperation == .above {
            return .move
        }
        return []
    }
    open func tableView(_ tableView: NSTableView, acceptDrop info: any NSDraggingInfo, row: Int, dropOperation: NSTableView.DropOperation) -> Bool {
        guard let dragItems = info.draggingPasteboard.pasteboardItems else { return false }
        let ls = dragItems.compactMap { $0.string(forType: .string) }
        let rowList: [Int] = ls.compactMap { $0.toInt }
        if rowList.isEmpty || rowList.contains(row) {
            return false
        }
        var newList: [M] = []
        for i in self.items.indices {
            if i == row {
                for a in rowList {
                    newList.append(items[a])
                }
                newList.append(items[row])
            } else {
                if !rowList.contains(i) {
                    newList.append(items[i])
                }
            }
        }
        //拖到最后一行下面了,  row == items.count
        if row >= self.items.count {
            for a in rowList {
                newList.append(items[a])
            }
        }
        self.items = newList
        if let b = onOrderChanged {
            b()
        }
        return true
    }

    public var itemCount: Int {
        items.count
    }
    public var selectedItems: [M] {
        var ls = [M]()
        let ns = self.tableView.selectedRowIndexes
        for i in ns {
            ls.append(self.getItem(i))
        }
        return ls
    }

    public func getItem(_ n: Int) -> M {
        items[n]
    }

    public func requestItems() {
        Tasks.back {
            let ls = self.onRequestItems()
            Tasks.fore {
                self.items = ls
            }
        }
    }

    open func onRequestItems() -> [M] {
        return items
    }

    public func menuItem(title: String, ident: Ident? = nil, _ block: @escaping (TableContextMenuItem) -> Void) {
        let tableMenu: NSMenu
        if let m = self.tableView.menu {
            tableMenu = m
        } else {
            tableMenu = NSMenu(title: "table context menu")
            tableMenu.delegate = self
            self.tableView.menu = tableMenu
        }

        let item = TableContextMenuItem(title: title, ident: ident)
        self.contextMenuItems += item
        item.menuItem = tableMenu.item(ident: item.ident, title: item.title) {
            $0.target = self
            $0.action = #selector(self.onContextMenuItemAction(_:))
        }
        block(item)
    }

    @objc
    private func onContextMenuItemAction(_ item: NSMenuItem) {
        self.contextMenuItems.first {
            $0.ident == item.identifier
        }?.onAction?(tableView.selectedRowSet)
    }

    @objc
    open func menuNeedsUpdate(_ menu: NSMenu) {
        menu.autoenablesItems = false
        for item in menu.items {
            let b = self.contextMenuItems.first {
                $0.ident == item.identifier
            }?.onValidate?(tableView.selectedRowSet)
            item.isEnabled = b ?? true
        }
    }

    private func find(col: Ident) -> HareTableColumn<M>? {
        colList.first {
            $0.ident == col
        }
    }

    public func column(title: String, width: CGFloat = 100, _ block: (HareTableColumn<M>) -> Void) {
        let c = NSTableColumn(identifier: Ident(title))
        c.title = title
        c.width = width
        c.resizingMask = [.userResizingMask]
        let ci = HareTableColumn<M>(c)
        block(ci)
        if ci.sorter != nil {
            c.sortDescriptorPrototype = NSSortDescriptor(key: c.identifier.rawValue, ascending: true)
        }
        self.tableView.addTableColumn(c)
        colList += ci
    }

    public func column(ident: Ident, width: CGFloat = 100, _ block: (HareTableColumn<M>) -> Void) {
        let c = NSTableColumn(identifier: ident)
        c.title = ident.rawValue
        c.width = width
        c.resizingMask = [.userResizingMask]
        let ci = HareTableColumn<M>(c)
        block(ci)
        if ci.sorter != nil {
            c.sortDescriptorPrototype = NSSortDescriptor(key: ident.rawValue, ascending: true)
        }
        self.tableView.addTableColumn(c)
        colList += ci
    }

    @objc
    open func tableView(_ tableView: NSTableView, sortDescriptorsDidChange oldDescriptors: [NSSortDescriptor]) {
        let ls = tableView.sortDescriptors
        if let a = ls.first, let k = a.key {
            if let c = find(col: Ident(k)) {
                if let ls = c.sorter?(items, a.ascending) {
                    self.items = ls
                }
            }
        }
    }

    open func onNewView(row: Int, col: NSUserInterfaceItemIdentifier) -> NSView {
        find(col: col)?.viewCreator?() ?? defaultLabelCreator()
    }

    open func onBindView(view: NSView, row: Int, col: NSUserInterfaceItemIdentifier) {
        find(col: col)?.viewBinder?(view, row, getItem(row))
    }

    @objc
    open func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        if let c = tableColumn {
            let v =
                tableView.makeView(withIdentifier: c.identifier, owner: nil)
                ?? onNewView(row: row, col: c.identifier).apply {
                    $0.identifier = c.identifier
                }
            onBindView(view: v, row: row, col: c.identifier)
            return v
        } else {
            return nil
        }
    }

    //    public func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
    //        heightMap[row] ?? self.tableView.rowHeight
    //    }

    @objc
    open func numberOfRows(in tableView: NSTableView) -> Int {
        itemCount
    }

    //double click
    public func onDoubleClick(_ block: @escaping (TableIndex) -> Void) {
        doubleClickCallback = block
        self.tableView.doubleAction = #selector(onDoubleClickItem(_:))
    }

    @objc
    private func onDoubleClickItem(_ sender: AnyObject?) {
        let r = self.tableView.clickedRow
        let c = self.tableView.clickedColumn
        if r >= 0 && c >= 0 {
            doubleClickCallback?(TableIndex(row: r, col: c))
        }
    }

}

public class HareTableColumn<T: Any>: NSObject {
    public var column: NSTableColumn
    public var ident: Ident {
        column.identifier
    }
    fileprivate var sorter: (([T], Bool) -> [T])? = nil
    fileprivate var viewCreator: (() -> NSView)? = nil
    fileprivate var viewBinder: ((NSView, Int, T) -> Void)? = { v, n, item in
        if let edit = v as? NSTextField {
            edit.stringValue = String(describing: item)
        }
    }

    private var onButtonClickCallback: ((Int, NSButton) -> Void)? = nil

    fileprivate init(_ col: NSTableColumn) {
        self.column = col
        super.init()
    }

    public var width: CGFloat {
        get {
            column.width
        }
        set {
            column.width = newValue
        }
    }
    public var title: String {
        get {
            column.title
        }
        set {
            column.title = newValue
        }
    }

    public func autoResize() {
        column.resizingMask = [.userResizingMask, .autoresizingMask]
    }

    public func sortBy<V: Comparable>(_ block: @escaping (T) -> V) {
        self.sorter = { ls, a in
            ls.sorted(asc: a, block)
        }
    }

    public func onNewView(onNew: @escaping () -> NSView) {
        viewCreator = onNew
    }

    public func onBindView(onBind: @escaping (NSView, Int, T) -> Void) {
        viewBinder = onBind
    }

    public func itemImage(onBind: @escaping (T) -> NSImage?) {
        onNewView {
            NSImageView(frame: .zero)
        }
        onBindView { view, idx, item in
            if let v = view as? NSImageView {
                v.image = onBind(item)
            }
        }
    }

    public func itemText(onBind: @escaping (T) -> String) {
        onBindView { itemView, row, item in
            if let lb = itemView as? NSTextField {
                lb.stringValue = onBind(item)
            } else if let a = item as? NSText {
                a.string = onBind(item)
            }
        }
    }

    public func itemSwitch(
        onChanged: ((Int, NSControl.StateValue) -> Void)? = nil,
        onChangeItem: ((T, Bool) -> Void)? = nil,
        onBind: @escaping (T) -> Bool
    ) {
        onNewView {
            NSSwitch(frame: .zero).apply {
                $0.tableRow = -1
                $0.onChanged(prop: "state") { ob in
                    if let sw = ob as? NSSwitch {
                        if sw.tableRow >= 0 {
                            onChanged?(sw.tableRow, sw.state)
                        }
                        if let item = sw.tableRowItem as? T {
                            onChangeItem?(item, sw.state == .on)
                        }
                    }
                }
            }
        }
        onBindView { itemView, row, item in
            if let sw = itemView as? NSSwitch {
                sw.tableRow = -1
                sw.tableRowItem = nil
                sw.state = onBind(item) ? .on : .off
                sw.tableRow = row
                sw.tableRowItem = item
            }
        }
    }

    public func itemButton(
        onClick: @escaping (Int, NSButton) -> Void,
        onBind: @escaping (NSButton, T) -> Void
    ) {
        onButtonClickCallback = onClick
        onNewView {
            NSButton(frame: .zero).apply {
                $0.bezelStyle = .texturedRounded
                $0.tableRow = -1
                $0.target = self
                $0.action = #selector(self.buttonColumnClick(_:))
            }
        }
        onBindView { itemView, row, item in
            if let btn = itemView as? NSButton {
                btn.tableRow = -1
                btn.tableRowItem = nil
                onBind(btn, item)
                btn.tableRow = row
                btn.tableRowItem = item
            }
        }
    }

    @objc
    private func buttonColumnClick(_ btn: NSButton) {
        let row = btn.tableRow
        if row >= 0 {
            onButtonClickCallback?(row, btn)
        }
    }

}

extension AttrKey {
    fileprivate static let tableRow: AttrKey = .init("HareTableRow")
    fileprivate static let tableRowItem: AttrKey = .init("HareTableRowItem")
}

extension NSControl {
    fileprivate var tableRow: Int {
        get {
            self[.tableRow] ?? -1
        }
        set {
            self[.tableRow] = newValue
        }
    }
    fileprivate var tableRowItem: Any? {
        get {
            self[.tableRowItem]
        }
        set {
            self[.tableRowItem] = newValue
        }
    }
}

extension NSTableView {
    public var selectedCount: Int {
        selectedRows.count
    }
    public var selectedRowSet: Set<Int> {
        selectedRows.toSet
    }
    public var selectedRows: [Int] {
        if self.allowsMultipleSelection {
            return self.selectedRowIndexes.map {
                $0
            }.filter {
                $0 >= 0
            }
        } else {
            let n = self.selectedRow
            if n >= 0 {
                return [n]
            }
            return []
        }
    }

    public func sortBy(ident: Ident, asc: Bool = true) {
        self.sortDescriptors = [NSSortDescriptor(key: ident.rawValue, ascending: asc)]
    }
}

extension NSTableView {
    @discardableResult
    public func column(ident: Ident, title: String, width: Int, resizing: NSTableColumn.ResizingOptions = [.userResizingMask], sortable: Bool = false) -> NSTableColumn {
        let c = NSTableColumn(identifier: ident)
        c.title = title
        c.width = CGFloat(width)
        c.resizingMask = resizing
        self.addTableColumn(c)
        if sortable {
            c.sortDescriptorPrototype = NSSortDescriptor(key: ident.rawValue, ascending: true)
        }
        return c
    }
}
