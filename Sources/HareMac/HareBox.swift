//
// Created by entaoyang@163.com on 2022/9/27.
//

import AppKit
import Foundation

open class HareBox: NSView {
    public var backgroundColor: NSColor = .windowBackgroundColor
    public var borderColor: NSColor = .placeholderTextColor
    public var borderWidth: CGFloat = 1
    public var cornerRadius: CGFloat = 8

    open override func draw(_ dirtyRect: NSRect) {
        let rect = self.bounds
        let path = NSBezierPath(roundedRect: rect, xRadius: cornerRadius, yRadius: cornerRadius)
        path.addClip()
        backgroundColor.setFill()
        path.fill()
        path.lineWidth = borderWidth
        borderColor.set()
        path.stroke()

        super.draw(dirtyRect)
    }
}

