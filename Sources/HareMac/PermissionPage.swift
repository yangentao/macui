//
//  PermissionPage.swift
//  HareMac
//
//  Created by Entao on 2025/1/28.
//
import AppKit
import Foundation
import HareSwift
import UniformTypeIdentifiers

@MainActor
public func authorizeDirectory(url: URL) async -> URL? {
    let p = AuthorizePage(url: url)
    NSApp.show(page: p, level: .floating)
    return await p.result
}
public class AuthorizePage: HarePage {
    private var url: URL
    private let resultLabel: NSTextField = LabelText(label: "", fontSize: 13)
    private let asyncResult: AsyncResult<URL> = .init()

    public init(url: URL) {
        self.url = url
        super.init()
        self.title = "Authorize".localHareMac
    }

    public required init?(coder: NSCoder) {
        fatalError()
    }
    public var result: URL? {
        get async {
            return await asyncResult.get()
        }
    }
    public override func windowWillClose(_ notification: Notification) {
        asyncResult.setAbsent(result: nil)
    }
    public override func viewDidLoad() {
        self.view.frame = .init(x: 0, y: 0, width: 400, height: 300)

        self.view.label("authTip".localHareMac).anchors { a in
            a.center(0, -30)
            a.height(30)
            a.keepContent(.horizontal)
        }
        let btn = NSButton(title: "openAuth".localHareMac, target: nil, action: #selector(onOpenDialog(_:)))
        self.view.view(btn).anchors { a in
            a.centerX()
            a.belowPre(8)
            a.height(24)
            a.keepContent(.horizontal)
        }
        self.view.view(resultLabel).anchors { a in
            a.centerX()
            a.belowPre(8)
            a.height(30)
            a.keepContent(.horizontal)
        }

        self.window?.orderFront(nil)
    }
    @objc
    func onOpenDialog(_ sender: Any?) {
        Task {
            if let retUrl = await OpenAuthorizeDialog(url: url, level: .floating) {
                resultLabel.stringValue = retUrl.path
                asyncResult.set(result: retUrl)
                self.close()
            }
        }
    }
    //    @MainActor
    //    func waitResult() async {
    //        if let retUrl = await OpenAuthorizeDialog(url: url, level: .floating) {
    //            resultLabel.stringValue = retUrl.path
    //            self.result = retUrl
    //            self.close()
    //        }
    //    }

}

@MainActor
public func OpenAuthorizeDialog(url: URL, level: NSWindow.Level? = nil) async -> URL? {
    let result = await OpenPanelSingleDirectory(url: url, title: "Folder Authorization".localHareMac, prompt: "Authorize".localHareMac, level: level)
    result?.bookmarkSave()
    return result
}
