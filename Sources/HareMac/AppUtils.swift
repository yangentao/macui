//
// Created by entaoyang@163.com on 2022/9/14.
//

import AppKit
import Foundation

extension NSViewController {
    public var window: NSWindow? {
        self.view.window
    }
}

private var AppName: String = {

    if let s = Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String, !s.isEmpty {
        return s
    }
    if let s = Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String, !s.isEmpty {
        return s
    }
    return ProcessInfo.processInfo.processName
}()

extension NSApplication {

    public var appName: String {
        AppName
    }
}

public var isDarkModel: Bool {
    if #available(macOS 10.14, *) {
        if let a = NSAppearance.currentDrawing().bestMatch(from: [NSAppearance.Name.aqua, NSAppearance.Name.darkAqua]) {
            return a == NSAppearance.Name.darkAqua
        }
    }
    return false
}

@MainActor
@discardableResult
public func ChangeApplication(status: AppStatus) async -> OSStatus {
    var psn = ProcessSerialNumber(highLongOfPSN: 0, lowLongOfPSN: kCurrentProcess.uint32Value)
    var t: Int
    switch status {
    case .foreground:
        t = kProcessTransformToForegroundApplication
    case .background:
        t = kProcessTransformToBackgroundApplication
    case .uielement:
        t = kProcessTransformToUIElementApplication
    }
    return TransformProcessType(&psn, t.uint32Value)
}
public enum AppStatus {
    case foreground, uielement, background
}
