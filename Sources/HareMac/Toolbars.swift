//
// Created by entaoyang@163.com on 2022/9/24.
//

import AppKit
import Foundation
import HareSwift

@objc
public protocol ToolbarActionCallback: AnyObject {
    func onToolbarAction(ident: Ident, index: Int)
}

public func buildTools(_ block: (ToolsBuilder) -> Void) -> [NSToolbarItem] {
    ToolsBuilder().apply(block).items
}

public class ToolsBuilder: NSObject {
    public var items: [NSToolbarItem] = []

    @discardableResult
    public func add<T: NSToolbarItem>(item: T) -> T {
        items += item
        return item
    }

    @discardableResult
    public func itemView(ident: Ident, label: String, view: NSView) -> NSToolbarItem {
        let c = ActionControl(frame: CGRect(width: 32, height: 32))
        let sz = view.fittingSize
        c.add(view: view).anchors { a in
            a.center()
            a.size(sz)
        }

        let a = NSToolbarItem(ident: ident, label: label)
        a.view = c
        return add(item: a)
    }

    @discardableResult
    public func itemGroup(ident: Ident, _ block: (ToolsBuilder) -> Void) -> NSToolbarItemGroup {
        let g = NSToolbarItemGroup(itemIdentifier: ident.identTool)
        g.subitems = buildTools(block)
        return add(item: g)
    }

    @discardableResult
    public func control(ident: Ident, label: String, control: NSControl) -> NSToolbarItem {
        let a = NSToolbarItem(ident: ident, label: label)
        a.view = control
        control.identifier = ident
        return add(item: a)
    }

    @discardableResult
    public func segment(ident: Ident, label: String, items: [(image: NSImage, tip: String)], block: ((NSSegmentedControl) -> Void)? = nil) -> NSToolbarItem {
        let ms = items.map {
            $0.image
        }
        let b = NSSegmentedControl(images: ms, trackingMode: .selectOne, target: nil, action: nil)
        for i in 0..<items.count {
            b.setToolTip(items[i].tip, forSegment: i)
        }
        b.selectedSegment = 0
        block?(b)
        return self.control(ident: ident, label: label, control: b)
    }

    @discardableResult
    public func button(ident: Ident, label: String, image: NSImage, block: ((NSButton) -> Void)? = nil) -> NSToolbarItem {
        let b = NSButton(image: image, target: nil, action: nil)
        b.bezelStyle = .texturedRounded
        block?(b)
        return self.control(ident: ident, label: label, control: b)
    }

    @discardableResult
    public func menu(ident: Ident, label: String, image: NSImage, _ block: (NSMenu) -> Void) -> NSMenuToolbarItem {
        let m = NSMenu(title: label)
        block(m)
        return self.menu(ident: ident, label: label, image: image, menu: m)
    }

    @discardableResult
    public func menu(ident: Ident, label: String, image: NSImage?, menu: NSMenu) -> NSMenuToolbarItem {
        let b = NSMenuToolbarItem(ident: ident, label: label, image: image)
        b.menu = menu
        return add(item: b)
    }

    @discardableResult
    public func item(ident: Ident, label: String, image: NSImage?) -> NSToolbarItem {
        let a = NSToolbarItem(ident: ident, label: label, image: image)
        return add(item: a)
    }

    @discardableResult
    public func space() -> NSToolbarItem {
        let a = NSToolbarItem(itemIdentifier: NSToolbarItem.Identifier.space)
        return add(item: a)

    }

    @discardableResult
    public func flexSpace() -> NSToolbarItem {
        let a = NSToolbarItem(itemIdentifier: NSToolbarItem.Identifier.flexibleSpace)
        return add(item: a)
    }
}

extension NSToolbarItem {
    public convenience init(ident: Ident, label: String, image: NSImage? = nil) {
        self.init(itemIdentifier: ident.identTool)
        self.label = label
        self.toolTip = label
        self.paletteLabel = label
        self.image = image
    }
}
public func ToolButton(_ ident: Ident, _ title: String, _ image: NSImage) -> NSToolbarItem {
    NSToolbarItem(ident: ident, label: title, image: nil).apply {
        $0.view = NSButton(image: image, target: nil, action: nil).apply {
            $0.bezelStyle = .texturedRounded
            $0.id = ident
        }
    }
}

extension NSWindow: @retroactive NSToolbarDelegate {
    fileprivate var toolbarItems: [NSToolbarItem] {
        get {
            return getAttr(key: "toolbarItems") ?? []
        }
        set {
            setAttr(key: "toolbarItems", value: newValue)
        }
    }

    public func addToolItems(_ list: [NSToolbarItem]) {
        if list.isEmpty { return }
        toolbarItems.removeAll(where: { item in
            list.contains(where: { a in a.itemIdentifier == item.itemIdentifier })
        })
        toolbarItems.append(contentsOf: list)
        rebuildToolbar()
    }
    public func removeToolItems(_ list: [NSToolbarItem]) {
        if list.isEmpty { return }
        toolbarItems.removeAll(where: { item in
            list.contains(where: { a in a.itemIdentifier == item.itemIdentifier })
        })
        rebuildToolbar()
    }
    public var requiredToolbar: NSToolbar {
        if let tb: NSToolbar = self.toolbar {
            if tb.delegate !== self {
                tb.delegate = self
            }
            return tb
        }
        let tb = NSToolbar().apply { a in
            a.allowsUserCustomization = false
            a.autosavesConfiguration = false
            a.sizeMode = .regular
            a.displayMode = .iconOnly
        }
        tb.delegate = self
        self.toolbar = tb
        return tb
    }
    private func rebuildToolbar() {
        let tb = requiredToolbar
        let n = tb.items.count
        for _ in 0..<n {
            tb.removeItem(at: 0)
        }
        for it in toolbarItems {
            tb.insertItem(withItemIdentifier: it.itemIdentifier, at: 0)
        }
    }
    @objc
    public func toolbarAllowedItemIdentifiers(_ toolbar: NSToolbar) -> [NSToolbarItem.Identifier] {
        return toolbarItems.map { $0.itemIdentifier }
    }
    @objc
    public func toolbarDefaultItemIdentifiers(_ toolbar: NSToolbar) -> [NSToolbarItem.Identifier] {
        return toolbarItems.map { $0.itemIdentifier }
    }
    @objc
    public func toolbar(_ toolbar: NSToolbar, itemForItemIdentifier itemIdentifier: NSToolbarItem.Identifier, willBeInsertedIntoToolbar flag: Bool) -> NSToolbarItem? {
        return toolbarItems.first(where: { $0.itemIdentifier == itemIdentifier })
    }
}

extension HarePage {

    @discardableResult
    public func installTools(_ list: [NSToolbarItem]) -> Self {
        for item in list {
            installItemAction(item)
        }
        return self
    }

    private func installItemAction(_ item: NSToolbarItem) {
        if let m = item as? NSMenuToolbarItem {
            for mi in m.menu.items {
                installMenuItemAction(mi)
            }
        } else if let g = item as? NSToolbarItemGroup {
            for a in g.subitems {
                installItemAction(a)
            }
        } else if let v = item.view as? NSControl, v.action == nil {
            v.target = self
            v.action = #selector(onToolItemAction)
        } else if item.action == nil {
            item.target = self
            item.action = #selector(onToolItemAction)
        }

    }

    private func installMenuItemAction(_ item: NSMenuItem) {
        if let m = item.submenu {
            for mi in m.items {
                installMenuItemAction(mi)
            }
        } else if item.action == nil {
            item.target = self
            item.action = #selector(onToolItemAction)
        }

    }

    @objc
    private func onToolItemAction(_ sender: NSObject) {
        if let c = sender as? NSView, let ident = c.identifier {
            if let seg = c as? NSSegmentedControl {
                onToolbarAction(ident: ident, index: seg.selectedSegment)
            } else {
                onToolbarAction(ident: ident, index: 0)
            }
        } else if let mi = sender as? NSMenuItem {
            if let mid = mi.identifier {
                onToolbarAction(ident: mid, index: 0)
            }
        } else if let ti = sender as? NSToolbarItem {
            onToolbarAction(ident: ti.itemIdentifier.identUI, index: 0)
        } else {
            loge("miss toolbar action: \(sender)")
        }
    }

}

extension SideWindow {

    @discardableResult
    public func installTools(_ list: [NSToolbarItem]) -> Self {
        for item in list {
            installItemAction(item)
        }
        return self
    }

    private func installItemAction(_ item: NSToolbarItem) {
        if let m = item as? NSMenuToolbarItem {
            for mi in m.menu.items {
                installMenuItemAction(mi)
            }
        } else if let g = item as? NSToolbarItemGroup {
            for a in g.subitems {
                installItemAction(a)
            }
        } else if let v = item.view as? NSControl, v.action == nil {
            v.target = self
            v.action = #selector(onToolItemAction)
        } else if item.action == nil {
            item.target = self
            item.action = #selector(onToolItemAction)
        }

    }

    private func installMenuItemAction(_ item: NSMenuItem) {
        if let m = item.submenu {
            for mi in m.items {
                installMenuItemAction(mi)
            }
        } else if item.action == nil {
            item.target = self
            item.action = #selector(onToolItemAction)
        }

    }

    @objc
    private func onToolItemAction(_ sender: NSObject) {
        if let c = sender as? NSView, let ident = c.identifier {
            if let seg = c as? NSSegmentedControl {
                onToolbarAction(ident: ident, index: seg.selectedSegment)
            } else {
                onToolbarAction(ident: ident, index: 0)
            }
        } else if let mi = sender as? NSMenuItem {
            if let mid = mi.identifier {
                onToolbarAction(ident: mid, index: 0)
            }
        } else if let ti = sender as? NSToolbarItem {
            onToolbarAction(ident: ti.itemIdentifier.identUI, index: 0)
        } else {
            loge("miss toolbar action: \(sender)")
        }
    }

}
