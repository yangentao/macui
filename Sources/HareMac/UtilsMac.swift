//
// Created by entaoyang@163.com on 2022/9/24.
//

import Foundation

public func lstatOf(path: String) -> Darwin.stat {
    var st: Darwin.stat = Darwin.stat()
    lstat(path, &st)
    return st
}
public func statOf(path: String) -> Darwin.stat {
    var st: Darwin.stat = Darwin.stat()
    stat(path, &st)
    return st
}
extension Darwin.stat {
    public var isReg: Bool {
        (self.st_mode & S_IFMT) == S_IFREG
    }
    public var isDir: Bool {
        (self.st_mode & S_IFMT) == S_IFDIR
    }
    public var isSymlink: Bool {
        (self.st_mode & S_IFMT) == S_IFLNK
    }
    // symlinkTarget = stL.isSymlink ? try? FileManager.default.destinationOfSymbolicLink(atPath: path) : nil
}

extension String {
    public var localHareMac: String {
        NSLocalizedString(self, bundle: Bundle.module, comment: "")
    }
}

extension NSObject {

    public func onNotify(name: NSNotification.Name, target: Any, selector: Selector) {
        NotificationCenter.default.addObserver(target, selector: selector, name: name, object: self)
    }

    public func onNotify(name: NSNotification.Name, _ callback: @escaping (Notification) -> Void) {
        NotificationCenter.default.addObserver(forName: name, object: self, queue: nil, using: callback)
    }

}
public func RealHomeDirectory() -> URL? {
    guard let pw = getpwuid(getuid()) else { return nil }
    return URL(fileURLWithFileSystemRepresentation: pw.pointee.pw_dir, isDirectory: true, relativeTo: nil)
}

public final class AsyncResult<T> {
    private var _result: T? = nil
    private let sem = DispatchSemaphore(value: 0)
    public private(set) var hasResult: Bool = false

    public init() {}

    public var result: T? {
        assert(hasResult)
        return _result
    }

    public func get(mill: Int) async -> T? {
        if hasResult {
            return _result
        }
        let ok = await withCheckedContinuation { con in
            var ret = true
            if !hasResult {
                let r: DispatchTimeoutResult = sem.wait(timeout: .now() + Double(mill) / 1000)
                ret = r == .success
            }
            con.resume(returning: ret)
        }
        return ok ? _result : nil
    }

    @discardableResult
    public func get() async -> T? {
        if hasResult {
            return _result
        }
        await withCheckedContinuation { con in
            if !hasResult {
                sem.wait()
            }
            con.resume()
        }
        return _result
    }
    public func set(result value: T?) {
        self._result = value
        hasResult = true
        sem.signal()
    }
    public func setAbsent(result value: T?) {
        if !hasResult {
            set(result: value)
        }
    }
}
