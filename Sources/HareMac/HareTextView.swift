//
// Created by entaoyang@163.com on 2022/9/27.
//

import Foundation
import AppKit
import HareSwift

open class HareTextView: NSScrollView {
    public let textView = NSTextView(frame: .zero)

    public override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        hasVerticalScroller = true
        hasHorizontalScroller = false

        let contSize = self.contentSize
        textView.autoresizingMask = [.height, .width]
        textView.frame = NSRect(width: contSize.width, height: contSize.height)
        textView.minSize = contSize
        textView.maxSize = NSMakeSize(CGFloat(Float.greatestFiniteMagnitude), CGFloat(Float.greatestFiniteMagnitude))

        textView.textContainer?.containerSize = NSSize(width: contSize.width - 40, height: CGFloat(Float.greatestFiniteMagnitude))
        textView.textContainer?.widthTracksTextView = true
        textView.isVerticallyResizable = true
        textView.isHorizontallyResizable = true
        textView.isEditable = true
        textView.textContainer?.lineBreakMode = .byCharWrapping

        textView.font = .monospacedSystemFont(ofSize: 13, weight: .regular)

        self.documentView = textView
    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}