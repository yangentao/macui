//
// Created by entaoyang@163.com on 2022/9/27.
//

import AppKit
import Foundation

open class HareCollectionView<T: Any>: NSScrollView, NSCollectionViewDataSource, NSCollectionViewDelegate {
    public let reuseIdent = Ident("0")
    public let collectionView = NSCollectionView(frame: .zero)
    public var flowLayout: NSCollectionViewFlowLayout {
        self.collectionView.collectionViewLayout as! NSCollectionViewFlowLayout
    }
    public private(set) var itemsData: [T] = []

    public override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        let lay = NSCollectionViewFlowLayout()
        lay.itemSize = NSSize(width: 60, height: 60)
        lay.minimumLineSpacing = 10
        lay.minimumInteritemSpacing = 10
        lay.sectionInset = NSEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        self.collectionView.collectionViewLayout = lay

        self.collectionView.isSelectable = true
        self.collectionView.allowsMultipleSelection = false

        self.documentView = self.collectionView
        self.hasVerticalScroller = true
        collectionView.dataSource = self
        collectionView.delegate = self
        self.onInit()
    }

    //

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    open func onInit() {

    }

    public func registerItemView<V: NSCollectionViewItem>(_ type: V.Type) {
        self.collectionView.register(type, forItemWithIdentifier: self.reuseIdent)
    }

    open func setItems(_ data: [T]) {
        self.itemsData = data
        self.collectionView.reloadData()
        self.onReloadData()
    }

    open func getItem(_ n: Int) -> T {
        self.itemsData[n]
    }

    open func getItem(indexPath: IndexPath) -> T {
        self.itemsData[indexPath.item]
    }

    open func onReloadData() {

    }

    open func onBind(itemView: NSCollectionViewItem, index: IndexPath) {

    }

    @objc
    open func numberOfSections(in collectionView: NSCollectionView) -> Int {
        return 1
    }

    @objc
    open func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        itemsData.count
    }

    @objc
    open func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        let v = collectionView.makeItem(withIdentifier: reuseIdent, for: indexPath)
        onBind(itemView: v, index: indexPath)
        return v
    }

    @objc
    public func collectionView(_ collectionView: NSCollectionView, shouldSelectItemsAt indexPaths: Set<IndexPath>) -> Set<IndexPath> {
        return indexPaths
    }

    @objc
    public func collectionView(_ collectionView: NSCollectionView, shouldDeselectItemsAt indexPaths: Set<IndexPath>) -> Set<IndexPath> {
        return indexPaths
    }

}

open class BaseCollectionItem: NSCollectionViewItem {
    public var selectedBackgroundColor: NSColor = NSColor.systemBlue.withAlphaComponent(0.3)
    public var indexPath: IndexPath? = nil

    public var doubleClickCallback: ((IndexPath) -> Void)? = nil

    open override var isSelected: Bool {
        didSet {
            if isSelected {
                self.view.layer?.backgroundColor = selectedBackgroundColor.cgColor
            } else {
                self.view.layer?.backgroundColor = NSColor.clear.cgColor
            }
        }
    }

    open override func loadView() {
        self.view = NSView(frame: NSRect(x: 0, y: 0, width: 100, height: 100))
        self.view.wantsLayer = true
        self.view.layer?.masksToBounds = false
        self.view.layer?.cornerRadius = 4
    }

    open override func mouseDown(with event: NSEvent) {
        if event.clickCount > 1 {
            if let p = self.indexPath {
                doubleClickCallback?(p)
            }
        }
        super.mouseDown(with: event)
    }
}

open class DataCollectionItem: BaseCollectionItem {
    public let iv: NSImageView = NSImageView(frame: .zero)
    public let tv: Label = Label(frame: .zero)

    open var horM = 3
    open var topM = 3
    open var bottomM = 3
    open var contentM = 3

    open var textH: CGFloat {
        tv.lineHeightByFont
    }

    open override func loadView() {
        super.loadView()
        self.view.add(view: iv)
        self.view.add(view: tv)

        self.imageView = iv
        self.textField = tv

        tv.alignment = .center
        tv.wantsLayer = true
        tv.layer?.masksToBounds = false
        tv.lineBreakMode = .byCharWrapping
    }

    open override func viewDidLoad() {
        super.viewDidLoad()
        configImage(iv)
        configLabel(tv)
    }

    open func configLabel(_ tv: Label) {
        tv.anchors { a in
            a.left(CGFloat(horM))
            a.right(-CGFloat(horM))
            a.below(iv, CGFloat(contentM))
            a.keepContent(.vertical)
        }
        tv.stringValue = "Title"

    }

    open func configImage(_ iv: NSImageView) {
        iv.anchors { a in
            a.centerX()
            a.top(CGFloat(topM))
            a.bottom(-CGFloat(bottomM + contentM) - textH)
            a.width.equal(anchor: iv.heightAnchor, multi: 1)
        }
    }
}
