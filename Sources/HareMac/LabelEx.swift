//
// Created by yangentao on 2019/10/23.
// Copyright (c) 2019 yangentao. All rights reserved.
//

import Cocoa
import Foundation

public func LabelText(label: String, fontSize: CGFloat = 12, textColor: NSColor? = nil, singleLine: Bool = true) -> NSTextField {
    let ed: NSTextField = .init(labelWithString: label)
    ed.usesSingleLineMode = singleLine
    ed.font = .systemFont(ofSize: fontSize)
    if let c = textColor {
        ed.textColor = c
    }
    return ed
}

extension NSTextField {
    public func styleLabel() {
        self.isEditable = false
        self.isBordered = false
        self.backgroundColor = .clear
        self.usesSingleLineMode = true
    }
    public func styleTitle(_ fontSize: CGFloat = 15) {
        self.font = .systemFont(ofSize: fontSize, weight: .medium)
    }
}

public enum TextAlign {
    case center, start, end
}
open class Label: NSTextField {

    private let myCell: MyTextFieldCell = MyTextFieldCell()

    public convenience init(title: String) {
        self.init(labelWithString: title)
        styleTitle()
    }
    public convenience init(label: String) {
        self.init(labelWithString: label)
    }
    public var alignVertical: TextAlign {
        get {
            myCell.alignVertical
        }
        set {
            self.myCell.alignVertical = newValue
            self.needsDisplay = true
        }
    }

    public override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        self.isEditable = false
        self.isBordered = false
        self.backgroundColor = .clear
        self.cell = myCell
        self.stringValue = ""
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}

open class MyTextFieldCell: NSTextFieldCell {
    public var alignVertical: TextAlign = .center

    private func adjustFrame(_ frame: NSRect) -> NSRect {
        switch self.alignVertical {
        case .center:
            var titleRect = super.titleRect(forBounds: frame)
            let cellSize = self.cellSize(forBounds: frame)
            titleRect.origin.y += (titleRect.height - cellSize.height) / 2
            titleRect.size.height = cellSize.height
            return titleRect
        case .end:
            var titleRect = super.titleRect(forBounds: frame)
            let cellSize = self.cellSize(forBounds: frame)
            titleRect.origin.y += titleRect.height - cellSize.height
            titleRect.size.height = cellSize.height
            return titleRect
        default:
            return frame
        }
    }

    public override func edit(withFrame rect: NSRect, in controlView: NSView, editor textObj: NSText, delegate: Any?, event: NSEvent?) {
        super.edit(withFrame: self.adjustFrame(rect), in: controlView, editor: textObj, delegate: delegate, event: event)
    }

    public override func select(withFrame rect: NSRect, in controlView: NSView, editor textObj: NSText, delegate: Any?, start selStart: Int, length selLength: Int) {
        super.select(withFrame: self.adjustFrame(rect), in: controlView, editor: textObj, delegate: delegate, start: selStart, length: selLength)
    }

    public override func drawInterior(withFrame cellFrame: NSRect, in controlView: NSView) {
        super.drawInterior(withFrame: self.adjustFrame(cellFrame), in: controlView)
    }

}
