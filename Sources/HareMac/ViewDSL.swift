//
// Created by entaoyang@163.com on 2022/9/12.
//

import AppKit
import Foundation
import HareSwift

extension NSView {

    public func test() {

    }
    @discardableResult
    public func group() -> NSView {
        return add(type: NSView.self)
    }
    @discardableResult
    public func group(_ block: (NSView) -> Void) -> NSView {
        view(block)
    }
    @discardableResult
    public func view() -> NSView {
        return add(type: NSView.self)
    }
    @discardableResult
    func view<T: XView>(type: T.Type) -> T {
        add(type: type)
    }
    @discardableResult
    public func view<T: NSView>(_ child: T) -> T {
        self.add(view: child)
        return child
    }
    @discardableResult
    public func view<T: NSView>(_ block: (T) -> Void) -> T {
        add(type: T.self).apply(block)
    }

    @discardableResult
    public func view<T: NSView>(_ child: T, _ block: (T) -> Void) -> T {
        self.add(view: child).apply(block)
    }

    @discardableResult
    public func label(_ label: String) -> NSTextField {
        return self.view(LabelText(label: label))
    }

    @discardableResult
    public func stack(_ closure: (NSStackView) -> Void) -> NSStackView {
        stackVertical(closure)
    }
    @discardableResult
    public func stackHorizontal() -> NSStackView {
        let v = NSStackView(frame: .zero)
        v.orientation = .horizontal
        v.alignment = .centerY
        v.distribution = .fillEqually
        return view(v)
    }
    @discardableResult
    public func stackHorizontal(_ closure: (NSStackView) -> Void) -> NSStackView {
        let v = NSStackView(frame: .zero)
        v.orientation = .horizontal
        v.alignment = .centerY
        v.distribution = .fillEqually
        return view(v, closure)
    }
    @discardableResult
    public func stackVertical() -> NSStackView {
        let v = NSStackView(frame: .zero)
        v.orientation = .vertical
        v.alignment = .centerX
        v.distribution = .fillEqually
        return view(v)
    }
    @discardableResult
    public func stackVertical(_ closure: (NSStackView) -> Void) -> NSStackView {
        let v = NSStackView(frame: .zero)
        v.orientation = .vertical
        v.alignment = .centerX
        v.distribution = .fillEqually
        return view(v, closure)
    }
    @discardableResult
    public func button(title: String, action: Selector?) -> NSButton {
        view(NSButton(title: title, target: nil, action: action))
    }
    @discardableResult
    public func button(_ closure: (NSButton) -> Void) -> NSButton {
        view(NSButton(title: "Button"), closure)
    }
    @discardableResult
    public func edit(value: String? = nil, hint: String? = nil, fontSize: CGFloat = 14, textColor: NSColor? = nil, singleLine: Bool = true, formatter: Formatter? = nil)
        -> NSTextField
    {
        return view(Edit(value: value, hint: hint, fontSize: fontSize, textColor: textColor, singleLine: singleLine, formatter: formatter))
    }

    @discardableResult
    public func textField(_ closure: (NSTextField) -> Void) -> NSTextField {
        view(closure)
    }

    @discardableResult
    public func collectionView(_ closure: (NSCollectionView) -> Void) -> NSCollectionView {
        view(closure)
    }

    @discardableResult
    public func imageView(_ closure: (NSImageView) -> Void) -> NSImageView {
        view(closure)
    }

    @discardableResult
    public func hareBox() -> HareBox {
        view(type: HareBox.self)
    }

    @discardableResult
    public func box(_ closure: (NSBox) -> Void) -> NSBox {
        view(closure)
    }

    @discardableResult
    public func tabView(_ closure: (NSTabView) -> Void) -> NSTabView {
        view(closure)
    }

    @discardableResult
    public func combox(_ closure: (NSComboBox) -> Void) -> NSComboBox {
        view(closure)
    }

    public func flexRow() -> FlexRow {
        self.view(type: FlexRow.self)
    }
    public func flexColumn() -> FlexColumn {
        self.view(type: FlexColumn.self)
    }
}

extension NSTabView {
    public func tab(ident: String, label: String, _ block: (NSTabViewItem) -> Void) {
        let a = NSTabViewItem(identifier: "buildinTab")
        a.label = label
        self.addTabViewItem(a)
        block(a)
    }
}
