import AppKit
import Foundation
import HareSwift

public class OutlineViewX: NSOutlineView {
    public override func makeView(withIdentifier identifier: NSUserInterfaceItemIdentifier, owner: Any?) -> NSView? {
        let view = super.makeView(withIdentifier: identifier, owner: owner)
        if identifier == NSOutlineView.disclosureButtonIdentifier, let oldBtn = view as? NSButton {
            if oldBtn.tag == 999 {
                return oldBtn
            }
            let b = NSButton(image: "chevron.right".symbolImage(size: 16)!, target: nil, action: nil)
            b.alternateImage = "chevron.down".symbolImage(size: 16)
            b.frame = .sized(24, 24)
            b.identifier = identifier
            b.bezelStyle = .circular
            b.bezelColor = NSColor.clear
            b.isBordered = false
            b.setButtonType(.toggle)
            b.target = oldBtn.target
            b.action = oldBtn.action
            b.frame = .sized(20)
            b.tag = 999
            return b
        }
        return view
    }
}

@MainActor
public class OutlineTable<T: OutlineNode & Any>: NSScrollView, NSOutlineViewDelegate, NSOutlineViewDataSource, NSMenuDelegate {
    public let outView: OutlineViewX = .init(frame: .zero)
    private var colList: [OutlineColumn<T>] = []

    public var items: [T] = []
    {
        didSet {
            self.outView.reloadData()
            labelTip.isHidden = !items.isEmpty
        }
    }

    private var contextMenuItems: [OutlineContextMenuItem] = []
    private var heightMap: [Int: CGFloat] = [:]
    private let labelTip: NSTextField = LabelText(label: "No Contents", singleLine: false).apply { $0.alignment = .center }
    public var autoExpandItem: Bool = true
    public var onDoubleClickItem: ((T) -> Void)? = nil

    public var onDropFiles: ([URL]) -> Void = {
        println($0)
    }
    public var onDragItem: ((T) -> (any NSPasteboardWriting)?)? = nil
    public var onDropValidate: ((any NSDraggingInfo, Any?) -> NSDragOperation)? = nil

    public override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        self.hasVerticalScroller = true
        self.autohidesScrollers = true
        self.documentView = self.outView

        self.outView.gridStyleMask = .dashedHorizontalGridLineMask
        self.outView.rowSizeStyle = .custom
        self.outView.rowHeight = 48

        self.outView.dataSource = self
        self.outView.delegate = self

        outView.add(view: labelTip)
        labelTip.anchorBuilder { b in
            b.center()
            b.keepSize()
        }
        self.outView.registerForDraggedTypes(PROMISE_TYPES)
        self.outView.registerForDraggedTypes([.fileURL])
        self.outView.setDraggingSourceOperationMask(.copy, forLocal: false)

        self.outView.doubleAction = #selector(onDoubleClickItem(_:))

    }

    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    public var selectedItems: [T] {
        var ls: [T] = []
        for r in self.outView.selectedRows {
            if let a = getItem(r) {
                ls.add(a)
            }
        }
        return ls
    }
    public func getItem(_ row: Int) -> T? {
        return outView.item(atRow: row) as? T
    }

    public lazy var defaultLabelCreator: () -> NSView = { [weak self] in
        let a = Label(frame: .zero)
        a.alignment = .left
        a.alignVertical = .center
        a.font = .systemFont(ofSize: 15)
        a.maximumNumberOfLines = 10
        return a
    }
    public var emptyText: String {
        get {
            return labelTip.stringValue
        }
        set {
            labelTip.stringValue = newValue
        }
    }

    public func expandAll() {
        outView.expandItem(nil, expandChildren: true)
    }
    public func expandTop() {
        outView.expandItem(nil, expandChildren: false)
    }
    public var clicked: RowCol {
        RowCol(row: outView.clickedRow, col: outView.clickedColumn)
    }

    public func toggleExpand(_ item: T) {
        if item.children.notEmpty {
            if outView.isItemExpanded(item) {
                outView.collapseItem(item)
            } else {
                outView.expandItem(item)
            }
        }
    }

    @objc
    private func onDoubleClickItem(_ sender: AnyObject?) {
        let rc = self.clicked
        if rc.row >= 0 {
            if let item = outView.item(atRow: rc.row) as? T {
                if let c = onDoubleClickItem {
                    c(item)
                } else if autoExpandItem {
                    toggleExpand(item)
                }
            }
        }
    }
    public func menuItem(title: String, ident: Ident? = nil, _ block: @escaping (OutlineContextMenuItem) -> Void) {
        let tableMenu: NSMenu
        if let m = self.outView.menu {
            tableMenu = m
        } else {
            tableMenu = NSMenu(title: "table context menu")
            tableMenu.delegate = self
            self.outView.menu = tableMenu
        }

        let item = OutlineContextMenuItem(title: title, ident: ident)
        self.contextMenuItems += item
        item.menuItem = tableMenu.item(ident: item.ident, title: item.title) {
            $0.target = self
            $0.action = #selector(self.onContextMenuItemAction(_:))
        }
        block(item)
    }

    @objc
    private func onContextMenuItemAction(_ item: NSMenuItem) {
        self.contextMenuItems.first {
            $0.ident == item.identifier
        }?.onAction?(outView.selectedRowSet)
    }

    @objc
    open func menuNeedsUpdate(_ menu: NSMenu) {
        menu.autoenablesItems = false
        for item in menu.items {
            let b = self.contextMenuItems.first {
                $0.ident == item.identifier
            }?.onValidate?(outView.selectedRowSet)
            item.isEnabled = b ?? true
        }
    }
    public func column(title: String, width: CGFloat = 100, _ block: (OutlineColumn<T>) -> Void) {
        let c = NSTableColumn(identifier: Ident(title))
        c.title = title
        c.width = width
        c.resizingMask = [.userResizingMask]
        let ci = OutlineColumn<T>(c)
        block(ci)
        if ci.sorter != nil {
            c.sortDescriptorPrototype = NSSortDescriptor(key: c.identifier.rawValue, ascending: true)
        }
        self.outView.addTableColumn(c)
        colList += ci
    }

    public func column(ident: Ident, width: CGFloat = 100, _ block: (OutlineColumn<T>) -> Void) {
        let c = NSTableColumn(identifier: ident)
        c.title = ident.rawValue
        c.width = width
        c.resizingMask = [.userResizingMask]
        let ci = OutlineColumn<T>(c)
        block(ci)
        if ci.sorter != nil {
            c.sortDescriptorPrototype = NSSortDescriptor(key: ident.rawValue, ascending: true)
        }
        self.outView.addTableColumn(c)
        colList += ci
    }
    private func find(col: Ident) -> OutlineColumn<T>? {
        colList.first {
            $0.ident == col
        }
    }

    open func outlineView(_ outlineView: NSOutlineView, numberOfChildrenOfItem item: Any?) -> Int {
        if let a = item as? T {
            return a.count
        }
        return items.count
    }

    open func outlineView(_ outlineView: NSOutlineView, child index: Int, ofItem item: Any?) -> Any {
        if let g = item as? T {
            return g.children[index]
        }
        return items[index]
    }

    open func outlineView(_ outlineView: NSOutlineView, isItemExpandable item: Any) -> Bool {
        if let a = item as? T {
            return !a.empty
        }
        return false
    }

    public func sortProp<V: Comparable>(asc: Bool, _ block: (T) -> V) {
        self.sort(asc: asc, cmp: { block($0) < block($1) })
    }

    public func sort(asc: Bool, cmp: (T, T) -> Bool) {
        items.sort(by: { a, b in
            asc ? cmp(a, b) : cmp(b, a)
        })
        for var item in items {
            item.sort(asc: asc, cmp: { a, b in cmp(a as! T, b as! T) })
        }

    }

    open func outlineView(_ outlineView: NSOutlineView, sortDescriptorsDidChange oldDescriptors: [NSSortDescriptor]) {
        let ls = outView.sortDescriptors
        if let a = ls.first, let k = a.key {
            let asc = a.ascending
            if let c = find(col: Ident(k)) {
                if let cmp = c.sorter {
                    sort(asc: asc, cmp: cmp)
                    outlineView.reloadData()
                }
            }
        }
    }

    // 向外拖 1
    open func outlineView(_ outlineView: NSOutlineView, pasteboardWriterForItem item: Any) -> (any NSPasteboardWriting)? {
        return onDragItem?(item as! T)
    }
    // 向外拖 2
    open func outlineView(_ outlineView: NSOutlineView, draggingSession session: NSDraggingSession, willBeginAt screenPoint: NSPoint, forItems draggedItems: [Any]) {
    }
    // 向外拖 3
    open func outlineView(_ outlineView: NSOutlineView, draggingSession session: NSDraggingSession, endedAt screenPoint: NSPoint, operation: NSDragOperation) {
    }

    open func outlineView(_ outlineView: NSOutlineView, updateDraggingItemsForDrag draggingInfo: any NSDraggingInfo) {
    }

    open func outlineView(_ outlineView: NSOutlineView, validateDrop info: any NSDraggingInfo, proposedItem item: Any?, proposedChildIndex index: Int) -> NSDragOperation {
        //        println("drag validateDrop")
        return onDropValidate?(info, item) ?? .copy
        //        return .copy
    }

    open func outlineView(_ outlineView: NSOutlineView, acceptDrop info: any NSDraggingInfo, item: Any?, childIndex index: Int) -> Bool {
        let urls: [URL] = info.draggingPasteboard.readFileURLs()
        if urls.notEmpty {
            onDropFiles(urls)
        }
        return true
    }

    //----------------------------

    open func onNewView(col: NSUserInterfaceItemIdentifier, item: T) -> NSView {
        find(col: col)?.viewCreator?() ?? defaultLabelCreator()
    }

    open func onBindView(col: NSUserInterfaceItemIdentifier, item: T, view: NSView) {
        find(col: col)?.viewBinder?(view, item)
    }

    open func outlineView(_ outlineView: NSOutlineView, viewFor tableColumn: NSTableColumn?, item: Any) -> NSView? {
        if let c = tableColumn {
            let v =
                outlineView.makeView(withIdentifier: c.identifier, owner: nil)
                ?? onNewView(col: c.identifier, item: item as! T).apply {
                    $0.identifier = c.identifier
                }
            onBindView(col: c.identifier, item: item as! T, view: v)
            return v
        } else {
            return nil
        }
    }

    //    func outlineView(_ outlineView: NSOutlineView, rowViewForItem item: Any) -> NSTableRowView?

    //    func outlineView(_ outlineView: NSOutlineView, didAdd rowView: NSTableRowView, forRow row: Int)

    //    func outlineView(_ outlineView: NSOutlineView, didRemove rowView: NSTableRowView, forRow row: Int)

    //    func outlineView(_ outlineView: NSOutlineView, willDisplayCell cell: Any, for tableColumn: NSTableColumn?, item: Any)

    //    func outlineView(_ outlineView: NSOutlineView, shouldEdit tableColumn: NSTableColumn?, item: Any) -> Bool

    //     func selectionShouldChange(in outlineView: NSOutlineView) -> Bool

    //     func outlineView(_ outlineView: NSOutlineView, shouldSelectItem item: Any) -> Bool

    //     func outlineView(_ outlineView: NSOutlineView, selectionIndexesForProposedSelection proposedSelectionIndexes: IndexSet) -> IndexSet

    //     func outlineView(_ outlineView: NSOutlineView, shouldSelect tableColumn: NSTableColumn?) -> Bool

    //     func outlineView(_ outlineView: NSOutlineView, mouseDownInHeaderOf tableColumn: NSTableColumn)

    //     func outlineView(_ outlineView: NSOutlineView, didClick tableColumn: NSTableColumn)

    //    func outlineView(_ outlineView: NSOutlineView, didDrag tableColumn: NSTableColumn)

    // func outlineView(_ outlineView: NSOutlineView, tintConfigurationForItem item: Any) -> NSTintConfiguration?

    // func outlineView(_ outlineView: NSOutlineView, typeSelectStringFor tableColumn: NSTableColumn?, item: Any) -> String?

    // func outlineView(_ outlineView: NSOutlineView, nextTypeSelectMatchFromItem startItem: Any, toItem endItem: Any, for searchString: String) -> Any?

    // func outlineView(_ outlineView: NSOutlineView, shouldTypeSelectFor event: NSEvent, withCurrentSearch searchString: String?) -> Bool

    //     func outlineView(_ outlineView: NSOutlineView, shouldShowCellExpansionFor tableColumn: NSTableColumn?, item: Any) -> Bool

}
//NSFilePromiseReceiver

public final class OutlineTextNode: OutlineNode, ToString {
    public let item: String
    public var children: [OutlineTextNode]
    public init(item: String, children: [OutlineTextNode] = []) {
        self.item = item
        self.children = children
    }
    public func toString() -> String {
        return item
    }
}
