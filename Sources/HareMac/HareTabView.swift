//
// Created by entaoyang@163.com on 2022/9/27.
//

import Foundation
import AppKit
import HareSwift

public class HareTabView: NSView {
    public var backgroundColor: NSColor = .windowBackgroundColor {
        didSet {
            seg.backgroundColor = backgroundColor
        }
    }
    public var borderColor: NSColor = .placeholderTextColor
    public var borderWidth: CGFloat = 1
    public var cornerRadius: CGFloat = 8
    public let seg = HareSegmentView(frame: .zero)
    private var items: [HareTabItem] = []

    private let borderTopOffset: CGFloat = 12
    private let contentViewTopOffset: CGFloat = 24
    public var contentInset: NSEdgeInsets = NSEdgeInsets(top: 12, left: 12, bottom: 12, right: 12) {
        didSet {
            updateInsets()
        }
    }

    private func updateInsets() {
        let inset = self.contentInset
        for item in items {
            let v = item.contentView
            v.updateConstant(attr: .left, value: inset.left)
            v.updateConstant(attr: .top, value: contentViewTopOffset + inset.top)
            v.updateConstant(attr: .right, value: -inset.right)
            v.updateConstant(attr: .bottom, value: -inset.bottom)
        }

    }

    public override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        self.add(view: seg).anchors { a in
            a.top(0)
            a.centerX()
            a.width.ge(constant: 40)
            a.height.ge(constant: 20)
            a.keepContent(.horizontal)
        }
        seg.trackingMode = .selectOne
        seg.segmentStyle = .texturedRounded
        seg.segmentDistribution = .fill
        seg.backgroundColor = backgroundColor
        seg.font = .systemFont(ofSize: 14)
        seg.target = self
        seg.action = #selector(onSegChange(_:))
    }

    @objc
    private func onSegChange(_ segment: NSSegmentedControl) {
        let n = segment.selectedSegment
        for i in 0..<items.count {
            items[i].contentView.isHidden = i != n
        }
    }

    public func addTab(_ title: String, _ block: (NSView) -> Void) {
        let item = HareTabItem(title)
        addTab(item)
        block(item.contentView)
    }

    public func addTab(_ item: HareTabItem) {
        items.add(item)
        let idx: Int = seg.segmentCount
        seg.segmentCount += 1
        seg.setLabel(item.title, forSegment: idx)
        seg.setImage(item.image, forSegment: idx)
        item.contentView.isHidden = true
        self.view(item.contentView) { v in
            v.anchors { a in
                a.left(contentInset.left)
                a.right(-contentInset.right)
                a.top(contentViewTopOffset + contentInset.top)
                a.bottom(-contentInset.bottom)
            }
        }
        if seg.selectedSegment < 0 {
            seg.setSelected(true, forSegment: 0)
            items[0].contentView.isHidden = false
        }
    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    public override func draw(_ dirtyRect: NSRect) {
        let rect = self.bounds
        var newY: CGFloat = rect.origin.y// + topContentOffset
        if isFlipped {
            newY = rect.origin.y + borderTopOffset
        }
        let newHeight: CGFloat = max(0, rect.size.height - borderTopOffset)
        let newRect = NSRect(x: rect.origin.x, y: newY, width: rect.size.width, height: newHeight)
        let path = NSBezierPath(roundedRect: newRect, xRadius: cornerRadius, yRadius: cornerRadius)
        path.addClip()
        backgroundColor.setFill()
        path.fill()
        path.lineWidth = borderWidth
        borderColor.set()
        path.stroke()

        super.draw(dirtyRect)
    }

}

public class HareTabItem {
    public let contentView: NSView = NSView(frame: .zero)
    public let image: NSImage?
    public let title: String

    public init(_ title: String, _ image: NSImage? = nil) {
        self.title = title
        self.image = image
    }
}

public class HareSegmentView: NSSegmentedControl {
    public var backgroundColor: NSColor = .windowBackgroundColor {
        didSet {
            self.layer?.backgroundColor = backgroundColor.cgColor
            self.layer?.setNeedsDisplay()
        }
    }

    public override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        self.wantsLayer = true
        self.layer?.backgroundColor = backgroundColor.cgColor
        self.layer?.masksToBounds = true
        self.layer?.cornerRadius = 6
    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    public override func updateLayer() {
        self.layer?.backgroundColor = backgroundColor.cgColor
        super.updateLayer()
    }
}

public extension NSTableView {
    var selectedRowArray: [Int] {
        self.selectedRowIndexes.map {
            $0
        }
    }
}

public extension NSView {
    @discardableResult
    func tabViewHare(_ block: (HareTabView) -> Void) -> HareTabView {
        return self.add(type:HareTabView.self).apply(block )
    }
}
