//
// Created by entaoyang@163.com on 2022/9/24.
//

import AppKit
import Foundation
import HareSwift

public typealias SymbolConfig = NSImage.SymbolConfiguration

public func SymbolImage(_ symbol: String, color: NSColor? = nil, palette: [NSColor] = [], size: CGFloat? = nil, weight: NSFont.Weight? = nil, scale: NSImage.SymbolScale = .medium)
    -> NSImage?
{
    guard let img = NSImage(systemSymbolName: symbol, accessibilityDescription: nil) else { return nil }
    var config: SymbolConfig = SymbolConfig(scale: scale)
    if let sz = size {
        config = config.applying(SymbolConfig(pointSize: sz, weight: weight ?? .regular))
    }
    if let co = color {
        config = config.applying(SymbolConfig(hierarchicalColor: co))
    }
    if palette.notEmpty {
        config = config.applying(SymbolConfig(paletteColors: palette))
    }

    return img.withSymbolConfiguration(config)
}

extension String {

    public var named: NSImage? {
        NSImage(named: self)
    }

    public var symbol: NSImage? {
        NSImage(systemSymbolName: self, accessibilityDescription: nil)
    }
    public func symbolImage(color: NSColor? = nil, palette: [NSColor] = [], size: CGFloat? = nil, weight: NSFont.Weight? = nil, scale: NSImage.SymbolScale = .medium) -> NSImage? {
        guard let img = NSImage(systemSymbolName: self, accessibilityDescription: nil) else { return nil }
        var config: SymbolConfig = SymbolConfig(scale: scale)
        if let sz = size {
            config = config.applying(SymbolConfig(pointSize: sz, weight: weight ?? .regular))
        }
        if let co = color {
            config = config.applying(SymbolConfig(hierarchicalColor: co))
        }
        if palette.notEmpty {
            config = config.applying(SymbolConfig(paletteColors: palette))
        }

        return img.withSymbolConfiguration(config)
    }

}

extension NSImage {
    public convenience init?(symbol: String) {
        self.init(systemSymbolName: symbol, accessibilityDescription: nil)
    }

    @available(macOS 12.0, *)
    public func config(color: NSColor? = nil, palette: [NSColor] = [], size: CGFloat? = nil, weight: NSFont.Weight? = nil, scale: NSImage.SymbolScale = .medium) -> NSImage {
        var config: SymbolConfig = SymbolConfig(scale: scale)
        if let sz = size {
            config = config.applying(SymbolConfig(pointSize: sz, weight: weight ?? .regular))
        }
        if let co = color {
            config = config.applying(SymbolConfig(hierarchicalColor: co))
        }
        if palette.notEmpty {
            config = config.applying(SymbolConfig(paletteColors: palette))
        }

        return self.withSymbolConfiguration(config)!
    }

}

extension NSImage {
    public func changed(_ block: (ImageChangeParam) -> Void) -> NSImage {
        let p = ImageChangeParam()
        block(p)
        return changeImage(from: self, config: p)
    }

    public func square(fill: NSColor?, size: CGFloat?, tint: NSColor? = nil, corner: CGFloat = 0, strokeColor: NSColor? = nil, strokeWidth: CGFloat = 1, insets: CGFloat = 0)
        -> NSImage
    {
        changed { p in
            p.square = true
            p.fillColor = fill
            p.size = size ?? 0
            p.tintColor = tint
            p.corner = corner
            p.strokeColor = strokeColor
            p.stroke = strokeWidth
            p.inset = insets
        }
    }

    public func rounded(fill: NSColor?, size: CGSize?, tint: NSColor? = nil, corner: CGFloat = 0, strokeColor: NSColor? = nil, strokeWidth: CGFloat = 1, insets: EdgeInsets = .zero)
        -> NSImage
    {

        let rect: CGRect = .sized(size ?? self.size)
        var imgRect: CGRect = rect.inset(by: insets)
        var rawImg: NSImage = self

        if let tc = tint {
            let r = CGRect(origin: .zero, size: imgRect.size)
            let timg = NSImage(size: r.size)
            timg.lockFocus()
            NSGraphicsContext.current?.imageInterpolation = .high
            rawImg.draw(in: r, from: NSRect(origin: .zero, size: rawImg.size), operation: .copy, fraction: 1.0)
            tc.set()
            r.fill(using: .sourceAtop)
            timg.unlockFocus()
            rawImg = timg
        }

        let img = NSImage(size: rect.size)
        img.lockFocus()
        NSGraphicsContext.current?.imageInterpolation = .high
        let cornerPath = NSBezierPath(roundedRect: rect, xRadius: corner, yRadius: corner)
        cornerPath.addClip()
        if let fc = fill {
            fc.setFill()
            cornerPath.fill()
        }
        if let sc = strokeColor {
            let x = strokeWidth / 2
            let strokePath = NSBezierPath(roundedRect: rect.inset(by: .all(x)), xRadius: corner - x, yRadius: corner - x)
            sc.setStroke()
            strokePath.lineWidth = strokeWidth
            strokePath.stroke()
            let p3 = NSBezierPath(roundedRect: rect.inset(by: .all(strokeWidth)), xRadius: corner - strokeWidth, yRadius: corner - strokeWidth)
            p3.addClip()
            imgRect = imgRect.inset(by: .all(strokeWidth))
        }

        rawImg.draw(in: imgRect, from: NSRect(origin: .zero, size: rawImg.size), operation: .sourceOver, fraction: 1.0)
        img.unlockFocus()
        return img
    }

    public static func shapeRound(fill: NSColor?, size: CGSize = .all(24), corner: CGFloat = 0, strokeColor: NSColor? = nil, strokeWidth: CGFloat = 1, insets: EdgeInsets = .zero)
        -> NSImage
    {
        let img = NSImage(size: size)
        img.lockFocus()

        let rect: CGRect = .sized(size).inset(by: insets)
        if let fc = fill {
            fc.setFill()
            let p = NSBezierPath(roundedRect: rect, xRadius: corner, yRadius: corner)
            p.addClip()
            p.fill()
        }
        if let sc = strokeColor {
            let a = strokeWidth / 2
            let p2 = NSBezierPath(roundedRect: rect.inset(by: .all(a)), xRadius: corner - a, yRadius: corner - a)
            sc.setStroke()
            p2.lineWidth = strokeWidth
            p2.stroke()
        }
        img.unlockFocus()
        return img
    }

    public static func shapeRect(fill: NSColor?, size: CGSize = .all(24), strokeColor: NSColor? = nil, strokeWidth: CGFloat = 1, insets: EdgeInsets = .zero) -> NSImage {
        let img = NSImage(size: size)
        img.lockFocus()

        let rect: CGRect = .sized(size).inset(by: insets)
        if let fc = fill {
            fc.setFill()
            let p = NSBezierPath(rect: rect)
            p.addClip()
            p.fill()
        }
        if let sc = strokeColor {
            let p2 = NSBezierPath(rect: rect.inset(by: .all(strokeWidth / 2)))
            sc.setStroke()
            p2.lineWidth = strokeWidth
            p2.stroke()
        }
        img.unlockFocus()
        return img
    }

    public static func shapeOval(fill: NSColor?, size: CGSize = .all(24), strokeColor: NSColor? = nil, strokeWidth: CGFloat = 1, insets: EdgeInsets = .zero) -> NSImage {
        let img = NSImage(size: size)
        img.lockFocus()

        let rect: CGRect = .sized(size).inset(by: insets)
        if let fc = fill {
            fc.setFill()
            let p = NSBezierPath(ovalIn: rect)
            p.addClip()
            p.fill()
        }
        if let sc = strokeColor {
            let p2 = NSBezierPath(ovalIn: rect.inset(by: .all(strokeWidth / 2)))
            sc.setStroke()
            p2.lineWidth = strokeWidth
            p2.stroke()
        }
        img.unlockFocus()
        return img
    }

    @discardableResult
    public func tint(color: NSColor) -> NSImage {
        self.lockFocus()
        color.set()
        NSRect(origin: .zero, size: self.size).fill(using: .sourceAtop)
        self.unlockFocus()
        return self
    }

    @discardableResult
    public func tinted(_ color: NSColor?) -> NSImage {
        return transform(size: nil, color: color)
    }

    @discardableResult
    public func trans(size: CGFloat?, color: NSColor?) -> NSImage {
        var sz: NSSize? = nil
        if let w = size {
            if self.size.width > 0 {
                sz = NSSize(width: w, height: w * self.size.height / self.size.width)
            } else {
                sz = NSSize(width: w, height: w)
            }
        }
        return transform(size: sz, color: color)
    }

    @discardableResult
    public func transform(size newSize: NSSize?, color: NSColor?) -> NSImage {
        let sz = newSize ?? self.size
        let img = NSImage(size: sz)
        img.lockFocus()
        let newRect = NSRect(origin: .zero, size: sz)
        NSGraphicsContext.current?.imageInterpolation = .high
        self.draw(in: newRect, from: NSRect(origin: .zero, size: self.size), operation: .copy, fraction: 1.0)
        if let c = color {
            c.set()
            newRect.fill(using: .sourceAtop)
        }
        img.unlockFocus()
        return img
    }

    @discardableResult
    public func savePng(url: URL) -> Bool {
        if let data = self.tiffRepresentation {
            let bmp = NSBitmapImageRep(data: data)
            let png = bmp?.representation(using: .png, properties: [:])
            do {
                try png?.write(to: url, options: [.atomic])
                return true
            } catch {
                logd(error)
                return false
            }
        }
        return false
    }

    @discardableResult
    public func saveJpg(url: URL) -> Bool {
        if let data = self.tiffRepresentation {
            let bmp = NSBitmapImageRep(data: data)
            let jpg = bmp?.representation(using: .jpeg, properties: [:])
            do {
                try jpg?.write(to: url, options: [.atomic])
                return true
            } catch {
                logd(error)
                return false
            }
        }
        return false
    }
}

public func tintImage(from: NSImage, color: NSColor) -> NSImage {
    let rect = CGRect(origin: .zero, size: from.size)
    let timg = NSImage(size: rect.size)
    timg.lockFocus()
    NSGraphicsContext.current?.imageInterpolation = .high
    from.draw(in: rect, from: rect, operation: .copy, fraction: 1.0)
    color.set()
    rect.fill(using: .sourceAtop)
    timg.unlockFocus()
    return timg
}

public func changeImage(from: NSImage, config: ImageChangeParam) -> NSImage {
    let fromSize: CGSize = from.size
    let outRect: CGRect
    if config.square {
        if config.size > 0 {
            outRect = .init(width: config.size, height: config.size)
        } else {
            outRect = .init(width: fromSize.maxValue, height: fromSize.maxValue)
        }
    } else {
        if config.size > 0 {
            if fromSize.width > fromSize.height {
                outRect = .init(width: config.size, height: (config.size * fromSize.height / fromSize.width).ge(1)).integral
            } else {
                outRect = .init(width: (config.size * fromSize.width / fromSize.height).ge(1), height: config.size).integral
            }
        } else {
            outRect = .sized(fromSize)
        }
    }
    let fromImage: NSImage
    if let tc = config.tintColor {
        fromImage = tintImage(from: from, color: tc)
    } else {
        fromImage = from
    }

    var drawRect: CGRect = outRect

    let resultImage = NSImage(size: outRect.size)
    resultImage.lockFocus()
    NSGraphicsContext.current?.imageInterpolation = .high
    let cornerPath = NSBezierPath(roundedRect: outRect, xRadius: config.corner, yRadius: config.corner)
    cornerPath.addClip()
    if let fc = config.fillColor {
        fc.setFill()
        cornerPath.fill()
    }
    if let sc = config.strokeColor, config.stroke > 0 {
        let half = config.stroke / 2
        let strokePath = NSBezierPath(roundedRect: outRect.inset(by: .all(half)), xRadius: (config.corner - half).ge(0), yRadius: (config.corner - half).ge(0))
        sc.setStroke()
        strokePath.lineWidth = config.stroke
        strokePath.stroke()
        let p3 = NSBezierPath(roundedRect: outRect.inset(by: .all(config.stroke)), xRadius: (config.corner - config.stroke).ge(0), yRadius: (config.corner - config.stroke).ge(0))
        p3.addClip()
        drawRect = drawRect.inset(by: .all(config.stroke))
    }
    drawRect = drawRect.inset(by: .all(config.inset))
    let fromRect: CGRect = NSRect(origin: .zero, size: fromImage.size)
    let realRect: CGRect = fromRect.center(in: drawRect)

    if let sc = config.shadowColor {
        let shadow: NSShadow = .init()
        shadow.shadowColor = sc
        shadow.shadowBlurRadius = config.shadowBlur
        shadow.shadowOffset = .init(width: config.shadowX, height: config.shadowY)
        shadow.set()
    }

    fromImage.draw(in: realRect, from: fromRect, operation: .sourceOver, fraction: 1.0)
    resultImage.unlockFocus()
    return resultImage
}

public class ImageChangeParam: NSObject {
    public var square: Bool = false
    public var size: CGFloat = 0
    public var inset: CGFloat = 0
    public var corner: CGFloat = 0
    public var stroke: CGFloat = 0
    public var strokeColor: NSColor? = nil
    public var fillColor: NSColor? = nil
    public var tintColor: NSColor? = nil
    public var shadowColor: NSColor? = nil
    public var shadowX: CGFloat = 0
    public var shadowY: CGFloat = 0
    public var shadowBlur: CGFloat = 0

    public override init() {
        super.init()
    }

}
