//
// Created by entaoyang@163.com on 2022/9/27.
//

import Foundation
import AppKit

public extension NSTextField {

    @discardableResult
    func defaultEdit() -> NSTextField {
        self.usesSingleLineMode = true
        self.font = .systemFont(ofSize: 14)
        self.isBezeled = true
        self.bezelStyle = .roundedBezel
        return self
    }

    @objc
    override func performKeyEquivalent(with event: NSEvent) -> Bool {
        if event.modifierFlags.contains(NSEvent.ModifierFlags.command) {
            switch event.charactersIgnoringModifiers {
            case "x":
                return NSApp.sendAction(#selector(NSText.cut(_:)), to: self.window?.firstResponder, from: self)
            case "c":
                return NSApp.sendAction(#selector(NSText.copy(_:)), to: self.window?.firstResponder, from: self)
            case "v":
                return NSApp.sendAction(#selector(NSText.paste(_:)), to: self.window?.firstResponder, from: self)
            case "a":
                return NSApp.sendAction(#selector(NSText.selectAll(_:)), to: self.window?.firstResponder, from: self)
            default:
                break
            }
        }
        return super.performKeyEquivalent(with: event)
    }
}

public func Edit(value:String? = nil, hint:String? = nil, fontSize:CGFloat = 14, textColor:NSColor? = nil, singleLine:Bool = true, formatter :Formatter? = nil )->NSTextField{
    let ed = NSTextField(frame: .zero)
    ed.usesSingleLineMode = singleLine
    ed.font = .systemFont(ofSize: fontSize)
    ed.isBezeled = true
    ed.bezelStyle = .roundedBezel
    ed.placeholderString = hint
    ed.lineBreakMode = .byCharWrapping
    if let c = textColor {
        ed.textColor = c
    }
    if let v = value {
        ed.stringValue = v
    }
    ed.formatter = formatter
    return ed
}
