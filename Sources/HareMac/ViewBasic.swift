//
// Created by entaoyang@163.com on 2021/2/9.
//

import Foundation
import AppKit
import HareSwift

public typealias ViewClickBlock = (NSView) -> Void

open class FlipView: NSView {
    open override var isFlipped: Bool {
        true
    }
}
