//
// Created by entaoyang@163.com on 2022/9/22.
//

import AppKit
import Foundation
import HareSwift

public typealias BlockControl = (NSControl) -> Void

extension AttrKey {
    fileprivate static let controlOnClick: AttrKey = .init("control_on_click")
    fileprivate static let onClickOnly: AttrKey = .init("_on_click_only")
}

private class ControlActionStub: NSObject {
    private override init() {
        super.init()
    }

    @objc
    func onControlAction(_ sender: NSControl) {
        sender.onClickOnly?() ?? sender.onClickCallback?(sender)
    }

    static let shared: ControlActionStub = ControlActionStub()
}
extension NSSwitch {
    public var isOn: Bool {
        get {
            self.state == NSControl.StateValue.on
        }
        set {
            self.state = newValue ? NSControl.StateValue.on : NSControl.StateValue.off
        }
    }
}
extension NSControl {

    fileprivate var onClickOnly: VoidBlock? {
        get {
            self[.onClickOnly]
        }
        set {
            self[.onClickOnly] = newValue
        }
    }
    fileprivate var onClickCallback: BlockControl? {
        get {
            self[.controlOnClick]
        }
        set {
            self[.controlOnClick] = newValue
        }
    }
    //use weak ref in block
    public func onClick(_ block: @escaping VoidBlock) {
        self.target = ControlActionStub.shared
        self.action = #selector(ControlActionStub.onControlAction(_:))
        onClickOnly = block
    }

    //use weak ref in block

    public func onClickControl(_ block: @escaping BlockControl) {
        self.target = ControlActionStub.shared
        self.action = #selector(ControlActionStub.onControlAction(_:))
        onClickCallback = block
    }
}
