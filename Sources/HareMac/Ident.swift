//
// Created by entaoyang@163.com on 2022/9/12.
//

import Foundation
import AppKit
import HareSwift

public typealias Ident = NSUserInterfaceItemIdentifier

public extension Ident {
    var identTool: NSToolbarItem.Identifier {
        NSToolbarItem.Identifier(rawValue: self.rawValue)
    }
}

public extension NSToolbarItem.Identifier {
    var identUI: Ident {
        Ident(rawValue: self.rawValue)
    }
}

fileprivate var identCurrent: Int = 0

public extension Ident {
    static var generateIdent: Ident {
        identCurrent += 1
        return Ident(rawValue: "autoID\(identCurrent)")
    }
}

public extension String {
    var identUI: NSUserInterfaceItemIdentifier {
        NSUserInterfaceItemIdentifier(rawValue: self)
    }
    var identTool: NSToolbarItem.Identifier {
        NSToolbarItem.Identifier(rawValue: self)
    }
}

public extension NSView {
    var id: Ident? {
        get {
            self.identifier
        }
        set {
            self.identifier = newValue
        }
    }
    var requireID: Ident {
        if let ident = self.identifier {
            return ident
        }
        let ident = Ident.generateIdent
        self.identifier = ident
        return ident
    }

    func child(ident: Ident) -> NSView? {
        for v in self.subviews {
            if v.identifier == ident {
                return v
            }
        }
        return nil
    }

    func find(ident: Ident) -> NSView? {
        if ident == self.identifier {
            return self
        }
        for v in self.subviews {
            if v.identifier == ident {
                return v
            }
        }
        return nil
    }

    func findDeep(ident: Ident) -> NSView? {
        if ident == self.identifier {
            return self
        }
        for v in self.subviews {
            if let a = v.findDeep(ident: ident) {
                return a
            }
        }
        return nil
    }
}

//lazy var label: UILabel = IdentView("hello", self)

public func IdentView<T: NSView>(_ page: NSViewController, _ viewIdent: Ident) -> T {
    guard let v = page.view.findDeep(ident: viewIdent) else {
        fatalError("NO view identifier: \(viewIdent)")
    }
    return v as! T
}

public func IdentView<T: NSView>(_ parentView: NSView, _ viewIdent: Ident) -> T {
    guard let v = parentView.findDeep(ident: viewIdent) else {
        fatalError("NO view identifier: \(viewIdent)")
    }
    return v as! T
}