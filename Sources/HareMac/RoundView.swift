//
//  OutlineView.swift
//  HareMac
//
//  Created by Entao on 2025/1/21.
//
import AppKit
import Foundation

open class RoundView: NSView {
    public var fillColor: NSColor = .windowBackgroundColor
    public var borderColor: NSColor = .placeholderTextColor
    public var borderWidth: CGFloat = 1
    public var cornerRadius: CGFloat = 8

    open override func draw(_ dirtyRect: NSRect) {
        let rect = self.bounds
        let path = NSBezierPath(roundedRect: rect, xRadius: cornerRadius, yRadius: cornerRadius)
        path.addClip()
        fillColor.setFill()
        path.fill()
        path.lineWidth = borderWidth
        borderColor.set()
        path.stroke()

        super.draw(dirtyRect)
    }
}
