//
// Created by entaoyang@163.com on 2022/10/6.
//

import Foundation
import AppKit
import HareSwift

public func menuBuilder(title: String, @AnyBuilder _ block: AnyBuildBlock) -> NSMenu {
    let ls: [NSMenuItem] = block().listItem()
    return NSMenu(title: title).apply {
        $0.items = ls
    }
}

public func MenuItem(_ ident: Ident, _ title: String, key: String = "", image: NSImage? = nil, target: AnyObject? = nil, action: Selector? = nil) -> NSMenuItem {
    NSMenuItem(title: title, action: nil, keyEquivalent: key).apply {
        $0.identifier = ident
        $0.image = image
        $0.target = target
        $0.action = action
    }
}

public func MenuItemSeprator() -> NSMenuItem {
    NSMenuItem.separator()
}

public extension NSMenuItem {
    func children(@AnyBuilder _ block: AnyBuildBlock) -> NSMenuItem {
        self.submenu = NSMenu(title: self.title).apply {
            $0.items = block().listItem()
        }
        return self
    }
}

//

//func testMenuBuilder() {
//    let m = menuBuilder(title: "MainMenu") {
//        MenuItem(.App, "App")
//        MenuItem(.File, "File")
//        MenuItem(.Help, "Help")
//    }
//}

