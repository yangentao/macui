import AppKit
//
//  Shell.swift
//  HareMac
//
//  Created by Entao on 2024/12/26.
//
import Foundation
import HareSwift

public typealias ProcessConfig = (Process) -> Void

@discardableResult
public func Shell(current: URL?, cmd: String, shell: Shells = .sh) -> PipeProcess {
    return PipeProcess(current: current, path: shell.path, args: ["-c", cmd])
}

@discardableResult
public func Shell(current: URL?, args: [String], shell: Shells = .sh) -> PipeProcess {
    return PipeProcess(current: current, path: shell.path, args: args)
}

@discardableResult
public func open(current: URL?, args: [String]) -> ShellResult {
    return PipeProcess(current: current, path: "/usr/bin/open", args: args).launch()
}

public class PipeProcess {
    public let pipe = Pipe()
    public let process = Process()
    private let dataBuffer = DataBuffer()
    public private(set) var startTime: Date = .now
    public private(set) var endTime: Date = .now

    private var _onOutput: (@Sendable (Data) -> Void)? = nil

    public init(current: URL?, path: String, args: [String]) {
        process.currentDirectoryURL = current
        process.launchPath = path
        process.arguments = args
        process.standardOutput = pipe
        process.standardError = pipe
    }

    @discardableResult
    public func onOutput(_ block: (@Sendable @escaping (Data) -> Void)) -> PipeProcess {
        self._onOutput = block
        return self
    }
    @discardableResult
    public func config(_ block: (Process) -> Void) -> PipeProcess {
        block(process)
        return self
    }
    public var output: Data {
        dataBuffer.data
    }
    public var duration: Double {
        endTime.timeIntervalSince1970 - startTime.timeIntervalSince1970
    }
    @discardableResult
    public func launch() -> ShellResult {
        startTime = .now
        process.launch()
        let data = try? pipe.fileHandleForReading.readToEnd()
        dataBuffer.append(data: data)
        process.waitUntilExit()
        endTime = .now
        return ShellResult(process: process, output: output, seconds: duration)
    }
    @discardableResult
    public func launchAsync() async -> ShellResult {
        startTime = .now
        pipe.fileHandleForReading.readabilityHandler = { @Sendable  rh in
            let data = rh.availableData
            self.dataBuffer.append(data: data)
            self._onOutput?(data)
        }
        process.launch()
        await process.waitExit()
        pipe.fileHandleForReading.readabilityHandler = nil
        endTime = .now

        return ShellResult(process: process, output: output, seconds: duration)
    }
}

extension Process {
    public func waitExit() async {
        let result = AsyncResult<Bool>()
        self.terminationHandler = { @Sendable _ in
            result.set(result: true)
        }
        await result.get()
        self.terminationHandler = nil 
    }
}

private class DataBuffer {
    var data: Data
    init(data: Data? = nil) {
        self.data = data ?? Data()
    }
    func append(data: Data?) {
        if let d = data {
            self.data.append(d)
        }
    }
}
public enum Shells: String, RawRepresentable, CaseIterable {
    case sh = "/bin/sh"
    case zsh = "/bin/zsh"
    case bash = "/bin/bash"
}
extension Shells {
    public var path: String {
        return self.rawValue
    }
}
public class ShellResult: CustomStringConvertible {
    public let reason: Process.TerminationReason
    public let code: Int
    public let data: Data
    public let seconds: Double

    public init(reason: Process.TerminationReason, code: Int, output: Data, seconds: Double = 0) {
        self.reason = reason
        self.code = code
        self.data = output
        self.seconds = seconds
    }
    public init(process: Process, output: Data, seconds: Double = 0) {
        assert(!process.isRunning)
        self.data = output
        self.reason = process.terminationReason
        self.code = Int(process.terminationStatus)
        self.seconds = seconds
    }
    public var output: String? {
        return String(data: data, encoding: .utf8)
    }
    public var success: Bool {
        return reason == .exit && code == 0
    }
    public var description: String {
        return (output ?? "") + "\n\n" + "Success:\(success), Code: \(code), Reason: \(reason == .exit ? "exit" : "signal")"
    }
}
