//
// Created by entaoyang@163.com on 2022/10/2.
//

import AppKit
import Foundation
import HareSwift
import UniformTypeIdentifiers

extension NSPasteboard {
    public func readURLs() -> [URL] {
        self.readObjects(forClasses: [NSURL.self])?.compactMap {
            $0 as? URL
        } ?? []
    }

    public func readFileURLs() -> [URL] {
        self.readObjects(forClasses: [NSURL.self], options: [.urlReadingFileURLsOnly: NSNumber(value: true)])?.compactMap {
            $0 as? URL
        } ?? []
    }

    public func readImageURLs() -> [URL] {
        self.readObjects(forClasses: [NSURL.self], options: [.urlReadingContentsConformToTypes: NSImage.imageTypes])?.compactMap {
            $0 as? URL
        } ?? []
    }
}

extension NSDraggingInfo {

    public var enumFileURL: [URL] {
        enumURL.filter {
            $0.isFileURL
        }
    }

    public var enumURL: [URL] {
        let ls: [NSURL] = self.enumItem(forView: nil)
        return ls.map {
            $0 as URL
        }
    }

    public var enumString: [String] {
        let ls: [NSString] = self.enumItem(forView: nil)
        return ls.map {
            $0 as String
        }
    }

    public func enumItem<T: NSPasteboardReading>(forView: NSView?) -> [T] {
        var ls: [T] = []
        self.enumerateDraggingItems(for: forView, classes: [T.self]) { (item: NSDraggingItem, _, _) in
            if let a = item.item as? T {
                ls.add(a)
            }
        }
        return ls
    }
}

open class FileDropView: RoundView {

    public var onFileCallback: ([URL]) -> Void = { urls in
        print("Drop : ")
        print(urls)
    }

    public override init(frame: NSRect) {
        super.init(frame: frame)
        registerForDraggedTypes([.fileURL])
    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    @objc
    open override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation {
        if sender.draggingPasteboard.canReadObject(forClasses: [NSURL.self]) {
            return .copy
        }
        return []
    }

    @objc
    open override func prepareForDragOperation(_ sender: NSDraggingInfo) -> Bool {
        return true
    }

    @objc
    open override func performDragOperation(_ sender: NSDraggingInfo) -> Bool {
        let ls = sender.draggingPasteboard.readFileURLs()
        if ls.notEmpty {
            self.onFileCallback(ls)
        }
        return true
    }

}
