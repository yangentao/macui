//
//  AttrString.swift
//  HareMac
//
//  Created by Entao on 2024/12/23.
//


import Foundation
import AppKit
import HareSwift

public func attrString(_ block: (NSMutableAttributedString) -> Void) -> NSMutableAttributedString {
    NSMutableAttributedString().apply(block)
}

public extension NSMutableAttributedString {
    private var range: NSRange {
        NSRange(location: 0, length: self.length)
    }
    var paragraphStyle: NSMutableParagraphStyle? {
        get {
            self.attribute(.paragraphStyle, at: 0, effectiveRange: nil) as? NSMutableParagraphStyle
        }
        set {
            if let v = newValue {
                self.addAttribute(.paragraphStyle, value: v, range: range)
            } else {
                self.removeAttribute(.paragraphStyle, range: range)
            }
        }
    }
    var paraStyle: NSMutableParagraphStyle {
        if let p = self.paragraphStyle {
            return p
        }
        let p = NSMutableParagraphStyle()
        self.paragraphStyle = p
        return p
    }

    @discardableResult
    func lineSpace(_ n: CGFloat) -> Self {
        paraStyle.lineSpacing = n
        return self
    }

    func lineBreakMode(_ m: NSLineBreakMode) -> Self {
        paraStyle.lineBreakMode = m
        return self
    }

    @discardableResult
    func alignment(_ n: NSTextAlignment) -> Self {
        paraStyle.alignment = n
        return self
    }

    @discardableResult
    func paragraphSpacing(_ n: CGFloat) -> Self {
        paraStyle.paragraphSpacing = n
        return self
    }

    @discardableResult
    func firstLineHeadIndent(_ n: CGFloat) -> Self {
        paraStyle.firstLineHeadIndent = n
        return self
    }

    @discardableResult
    func foreColor(_ c: NSColor) -> Self {
        self.addAttribute(.foregroundColor, value: c, range: range)
        return self
    }

    @discardableResult
    func backColor(_ c: NSColor) -> Self {
        self.addAttribute(.backgroundColor, value: c, range: range)
        return self
    }

    @discardableResult
    func underlineStyle(_ b: NSUnderlineStyle) -> Self {
        self.addAttribute(.underlineStyle, value: b, range: range)
        return self
    }

    @discardableResult
    func underlineColor(_ c: NSColor) -> Self {
        self.addAttribute(.underlineColor, value: c, range: range)
        return self
    }

    @discardableResult
    func shadow(_ c: NSShadow) -> Self {
        self.addAttribute(.shadow, value: c, range: range)
        return self
    }

    @discardableResult
    func font(_ c: NSFont) -> Self {
        self.addAttribute(.font, value: c, range: range)
        return self
    }

}

private func makeAttachment(fontSize: CGFloat, image: NSImage) -> NSTextAttachment {
    let font = NSFont.systemFont(ofSize: fontSize) //set accordingly to your font, you might pass it in the function
//    let a = NSTextAttachment(image: image)
    let a = NSTextAttachment()
    a.image = image
    let y3 = (font.capHeight - image.size.height) / 2
    a.bounds = CGRect(x: 0, y: y3, width: image.size.width, height: image.size.height).integral
    return a
}

public extension String {
    var attred: NSMutableAttributedString {
        NSMutableAttributedString(string: self)
    }

    func with(color: NSColor) -> NSAttributedString {
        NSAttributedString(string: self, attributes: [.foregroundColor: color])
    }

    func with(image: NSImage?, size: CGFloat = 14) -> NSAttributedString {
        if let img = image {
            let a = makeAttachment(fontSize: size, image: img)
            return NSAttributedString(attachment: a)
        }
        return NSAttributedString(string: self)
    }
}

public func +(_ lhs: NSAttributedString, rhs: NSAttributedString) -> NSAttributedString {
    let a = NSMutableAttributedString()
    a.append(lhs)
    a.append(rhs)
    return a
}

public func +(_ lhs: String, rhs: NSAttributedString) -> NSAttributedString {
    let a = NSMutableAttributedString(string: lhs)
    a.append(rhs)
    return a
}

public func +(_ lhs: NSAttributedString, _ rhs: String) -> NSAttributedString {
    let a = NSMutableAttributedString()
    a.append(lhs)
    a.append(NSAttributedString(string: rhs))
    return a
}

public func +=(_ lhs: NSMutableAttributedString, rhs: NSAttributedString) {
    lhs.append(rhs)
}

public func +=(_ lhs: NSMutableAttributedString, rhs: String) {
    lhs.append(NSAttributedString(string: rhs))
}

public extension String {
    func htmlString(font: NSFont) -> NSAttributedString {
        let ss = try? NSMutableAttributedString(data: self.dataUnicode, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        if let sss = ss {
            sss.addAttribute(.font, value: font, range: NSRange(location: 0, length: sss.length))
            return sss
        }
        return NSAttributedString(string: self)
    }
}

public extension NSAttributedString {

    func linkfy() -> NSMutableAttributedString {
        let s = NSMutableAttributedString(attributedString: self)
        guard let detect = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue | NSTextCheckingResult.CheckingType.phoneNumber.rawValue) else {
            return s
        }
        let ms = detect.matches(in: s.string, range: NSRange(location: 0, length: s.length))
        for m in ms {
            if m.resultType == .link, let url = m.url {
                s.addAttributes([.link: url, .foregroundColor: NSColor.blue, .underlineStyle: NSUnderlineStyle.patternDot.rawValue], range: m.range)
            } else if m.resultType == .phoneNumber, let pn = m.phoneNumber {
                s.addAttributes([.link: pn, .foregroundColor: NSColor.blue, .underlineStyle: NSUnderlineStyle.patternDot.rawValue], range: m.range)
            }
        }
        return s
    }

}
