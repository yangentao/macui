import AppKit
import Foundation
import HareSwift

public struct RowCol {
    public let row: Int
    public let col: Int
    public let valid: Bool
    public init(row: Int, col: Int) {
        self.row = row
        self.col = col
        self.valid = row >= 0 && col >= 0
    }
}
public protocol OutlineNode {
    associatedtype NodeItemType
    var item: NodeItemType { get }
    var children: [Self] { get set }

}
extension OutlineNode {
    public var count: Int {
        children.count
    }
    public var empty: Bool {
        children.isEmpty
    }

    mutating public func sort(asc: Bool, cmp: (any OutlineNode, any OutlineNode) -> Bool) {
        if empty {
            return
        }
        children = children.sorted(by: { a, b in
            asc ? cmp(a, b) : cmp(b, a)
        })
        for var ch in children {
            ch.sort(asc: asc, cmp: cmp)
        }
    }
}
public class OutlineContextMenuItem {
    public let title: String
    public let ident: Ident
    public var onValidate: ((Set<Int>) -> Bool)? = nil
    public var onAction: ((Set<Int>) -> Void)? = nil
    public var menuItem: NSMenuItem? = nil

    public init(title: String, ident: Ident? = nil) {
        self.title = title
        self.ident = ident ?? Ident(title)
    }
}

public class OutlineColumn<T: OutlineNode>: NSObject {

    public var column: NSTableColumn
    public var ident: Ident {
        column.identifier
    }
    public var sorter: ((T, T) -> Bool)? = nil
    public var viewCreator: (() -> NSView)? = nil
    public var viewBinder: ((NSView, T) -> Void)? = { v, item in
        if let edit = v as? NSTextField {
            edit.stringValue = String(describing: item)
        }
    }

    private var onSwitchStateCallback: ((T, NSControl.StateValue) -> Void)? = nil
    private var onButtonClickCallback: ((T, NSButton) -> Void)? = nil

    public init(_ col: NSTableColumn) {
        self.column = col
        super.init()
    }

    public var width: CGFloat {
        get {
            column.width
        }
        set {
            column.width = newValue
        }
    }
    public var title: String {
        get {
            column.title
        }
        set {
            column.title = newValue
        }
    }

    public func autoResize() {
        column.resizingMask = [.userResizingMask, .autoresizingMask]
    }

    public func sortBy<V: Comparable>(_ block: @escaping (T) -> V) {
        self.sorter = { a, b in
            block(a) < block(b)
        }
    }

    public func onNewView(onNew: @escaping () -> NSView) {
        viewCreator = onNew
    }

    public func onBindView(onBind: @escaping (NSView, T) -> Void) {
        viewBinder = onBind
    }

    public func itemImage(onBind: @escaping (T) -> NSImage?) {
        onNewView {
            NSImageView(frame: .zero)
        }
        onBindView { view, item in
            if let v = view as? NSImageView {
                v.image = onBind(item)
            }
        }
    }
    public func itemText(align: NSTextAlignment, onBind: @escaping (T) -> String) {
        onBindView { itemView, item in
            if let lb = itemView as? NSTextField {
                lb.stringValue = onBind(item)
                lb.alignment = align
            } else if let a = item.item as? NSText {
                a.string = onBind(item)
                a.alignment = align
            }
        }
    }
    public func itemText(onBind: @escaping (T) -> String) {
        onBindView { itemView, item in
            if let lb = itemView as? NSTextField {
                lb.stringValue = onBind(item)
            } else if let a = item.item as? NSText {
                a.string = onBind(item)
            }
        }
    }

    public func itemSwitch(onChanged: ((T, NSControl.StateValue) -> Void)? = nil, onBind: @escaping (T) -> Bool) {
        self.onSwitchStateCallback = onChanged
        onNewView {
            NSSwitch(frame: .zero).apply {
                $0.outlineItem = nil
                $0.onChanged(prop: "state") { [weak self] ob in
                    if let sw = ob as? NSSwitch {
                        if let oi = sw.outlineItem {
                            self?.onSwitchStateCallback?(oi as! T, sw.state)
                        }
                    }
                }
            }
        }
        onBindView { itemView, item in
            if let sw = itemView as? NSSwitch {
                sw.outlineItem = nil
                sw.state = onBind(item) ? .on : .off
                sw.outlineItem = item
            }
        }
    }

    public func itemButton(onClick: @escaping (T, NSButton) -> Void, onBind: @escaping (NSButton, T) -> Void) {
        onButtonClickCallback = onClick
        onNewView {
            NSButton(frame: .zero).apply {
                $0.bezelStyle = .texturedRounded
                $0.outlineItem = nil
                $0.target = self
                $0.action = #selector(self.buttonColumnClick(_:))
            }
        }
        onBindView { itemView, item in
            if let btn = itemView as? NSButton {
                btn.outlineItem = nil
                onBind(btn, item)
                btn.outlineItem = item
            }
        }
    }

    @objc
    private func buttonColumnClick(_ btn: NSButton) {
        if let a = btn.outlineItem as? T {
            onButtonClickCallback?(a, btn)
        }
    }

}

extension AttrKey {
    fileprivate static let outlineItem: AttrKey = .init("outline.item")
}

extension NSControl {
    fileprivate var outlineItem: (any OutlineNode)? {
        get {
            self[.outlineItem]
        }
        set {
            self[.outlineItem] = newValue
        }
    }
}
