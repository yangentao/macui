//
// Created by entaoyang@163.com on 2022/9/13.
//

import Foundation
import AppKit

@objc
public protocol MenuActionCallback: AnyObject  {

    func onMenuAction(item: NSMenuItem)
}


public extension NSMenu {

    //onMenuAction(item:)
    @discardableResult
    func installActions(_ callback: NSObject & MenuActionCallback) -> Self {
        self.eachItem {
            if $0.action == nil {
                $0.target = callback
                $0.action = #selector(callback.onMenuAction(item:))
            }
        }
        return self
    }

    @discardableResult
    func installActions(target: AnyObject?, action: Selector) -> Self {
        self.eachItem {
            if $0.action == nil {
                $0.target = target
                $0.action = action
            }
        }
        return self
    }
}

public extension NSMenuItem {
    //onMenuAction(item:)
    @discardableResult
    func installAction(_ callback: some MenuActionCallback) -> Self {
        self.target = callback
        self.action = #selector(callback.onMenuAction(item:))
        return self
    }

    @discardableResult
    func subMenu(_ block: (NSMenu) -> Void) -> NSMenu {
        let m = NSMenu(title: self.title)
        self.submenu = m
        block(m)
        return m
    }
}

public func buildMenu(title: String, _ block: (NSMenu) -> Void) -> NSMenu {
    let m = NSMenu(title: title)
    block(m)
    return m
}

fileprivate typealias MenuItemAction = (NSMenuItem) -> Void


public extension NSMenu {
    func eachItem(_ block: (NSMenuItem) -> Void) {
        for item in self.items {
            if let m = item.submenu {
                m.eachItem(block)
            } else {
                block(item)
            }
        }
    }

    func findItem(_ ident: Ident) -> NSMenuItem? {
        for item in self.items {
            if item.identifier == ident {
                return item
            }
            if let m = item.submenu?.findItem(ident) {
                return m
            }
        }
        return nil
    }

    func itemSeprator() {
        addItem(NSMenuItem.separator())
    }

    @discardableResult
    func item(ident: Ident, title: String, block: ((NSMenuItem) -> Void)? = nil) -> NSMenuItem {
        let item = self.addItem(withTitle: title, action: nil, keyEquivalent: "")
        item.identifier = ident
        block?(item)
        return item
    }
}


public extension NSMenuItem {
    var tagA: Int {
        get {
            self.tag & 0xFFFF
        }
        set {
            self.tag = (self.tag & 0x7FFFFFFFFFFF0000) | (newValue & 0xFFFF)
        }
    }
    var tagB: Int {
        get {
            (self.tag >> 16) & 0xFFFF
        }
        set {
            self.tag = (self.tag & 0x7FFFFFFF0000FFFF) | ((newValue & 0xFFFF) << 16)
        }
    }
    var tagC: Int {
        get {
            (self.tag >> 32) & 0xFFFF
        }
        set {
            self.tag = (self.tag & 0x7FFF0000FFFFFFFF) | ((newValue & 0xFFFF) << 32)
        }
    }
    var tagD: Int {
        get {
            (self.tag >> 48) & 0xFFFF
        }
        set {
            self.tag = (self.tag & 0x0000FFFFFFFFFFFF) | ((newValue & 0xFFFF) << 48)
        }
    }
}



