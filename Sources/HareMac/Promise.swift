//
//  DragDrop.swift
//  HareZip
//
//  Created by Entao on 2025/1/22.
//

import AppKit
import Foundation
import HareSwift
import UniformTypeIdentifiers

public class PromiseInfo<T> {
    public let info: T
    public let url: URL
    public init(info: T, url: URL) {
        self.info = info
        self.url = url
    }
}
public typealias PromiseCallback<T> = (PromiseInfo<T>) throws -> Void
public let PROMISE_TYPES = NSFilePromiseReceiver.readableDraggedTypes.map({ NSPasteboard.PasteboardType($0) })

open class PromiseFile<T: Any>: NSFilePromiseProvider, NSFilePromiseProviderDelegate {
    public let info: T
    public let fileName: String
    private lazy var queue: OperationQueue = OperationQueue().apply {
        $0.maxConcurrentOperationCount = 1
        $0.qualityOfService = .utility
        $0.name = "PromiseFileQueue"
    }
    public var writeCallback: PromiseCallback<T>? = nil

    public init(info: T, fileName: String, type: UTType) {
        self.info = info
        self.fileName = fileName
        super.init()
        super.fileType = type.identifier
        super.userInfo = info
        super.delegate = self
        if fileName.contains("/") {
            errorX("文件名中不能包含路径")
        }
    }

    public func operationQueue(for filePromiseProvider: NSFilePromiseProvider) -> OperationQueue {
        self.queue
    }

    public func filePromiseProvider(_ filePromiseProvider: NSFilePromiseProvider, fileNameForType fileType: String) -> String {
        self.fileName
    }

    public func filePromiseProvider(_ filePromiseProvider: NSFilePromiseProvider, writePromiseTo url: URL, completionHandler: @escaping (Error?) -> Void) {
        do {
            let pi = PromiseInfo(info: info, url: url)
            try self.onWrite(pi)
            completionHandler(nil)
        } catch let error {
            loge(error)
            completionHandler(error)
        }
    }
    open func onWrite(_ promiseInfo: PromiseInfo<T>) throws {
        if let c = writeCallback {
            try c(promiseInfo)
            return
        }
        errorX("NOT Implement: onWrite(from: to: )")
    }

}
