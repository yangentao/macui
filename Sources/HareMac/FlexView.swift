//
//  FlexView.swift
//  HareMac
//
//  Created by Entao on 2025/1/2.
//

import AppKit
import Foundation
import HareSwift

public enum SizeValue: Codable {
    case wrap
    case weight(_ value: CGFloat)
    case fixed(_ value: CGFloat)
    case percent(_ value: CGFloat)
}

public class FlexParam: CustomStringConvertible, Codable {
    public var left: CGFloat?
    public var right: CGFloat?
    public var top: CGFloat?
    public var bottom: CGFloat?
    public var centerX: Bool?
    public var centerY: Bool?
    public var biasX: CGFloat?
    public var biasY: CGFloat?
    public var width: SizeValue?
    public var height: SizeValue?
    public var huggingPriorityLower: Bool

    init(
        left: CGFloat? = nil, right: CGFloat? = nil, top: CGFloat? = nil, bottom: CGFloat? = nil, centerX: Bool? = nil, centerY: Bool? = nil, biasX: CGFloat? = nil,
        biasY: CGFloat? = nil, width: SizeValue? = nil, height: SizeValue? = nil, huggingPriorityLower: Bool = true
    ) {
        self.left = left
        self.right = right
        self.top = top
        self.bottom = bottom
        self.centerX = centerX
        self.centerY = centerY
        self.biasX = biasX
        self.biasY = biasY
        self.width = width
        self.height = height
        self.huggingPriorityLower = huggingPriorityLower
    }

    public var description: String {
        return buildString(
            "ColumnParam: left: ", left, ", right: ", right, ", top: ", top, ", bottom: ", bottom, ", centerX: ", centerX, ", centerY: ", centerY,
            ", biasX: ", biasX, ", biasY: ", biasY, ", width: ", width, ", height: ", height
        )
    }
}

extension AttrKey {
    fileprivate static let _flexParam: AttrKey = .init("_flexParam_")
}

extension XView {
    public var flexParams: FlexParam? {
        get {
            self[._flexParam]
        }
        set {
            self[._flexParam] = newValue
            if let sv = self.superview as? FlexColumn {
                sv.needsRebuild()
            }
        }
    }

    @discardableResult
    public func flexParams(_ block: (FlexParam) -> Void) -> Self {
        if let p = self.flexParams {
            block(p)
            return self
        }
        let fp = FlexParam()
        block(fp)
        self.flexParams = fp
        return self
    }
    @discardableResult
    public func flexParam(
        left: CGFloat? = nil, right: CGFloat? = nil, top: CGFloat? = nil, bottom: CGFloat? = nil, centerX: Bool? = nil, centerY: Bool? = nil, biasX: CGFloat? = nil,
        biasY: CGFloat? = nil, width: SizeValue? = nil, height: SizeValue? = nil, huggingPriorityLower: Bool = true
    ) -> Self {
        self.flexParams = .init(
            left: left, right: right, top: top, bottom: bottom, centerX: centerX, centerY: centerY, biasX: biasX,
            biasY: biasY, width: width, height: height, huggingPriorityLower: huggingPriorityLower)
        return self
    }

}

public class FlexColumn: BaseLayoutView {
    private let heightGuide: XLayoutGuide = XLayoutGuide()
    public var space: CGFloat = 0
    public var itemParam: FlexParam = FlexParam(left: 0, top: 0, width: .wrap, height: .wrap)

    public override func onInit() {
        self.addLayoutGuide(heightGuide)
    }
    public override func onBuildConstraints() -> [NSLayoutConstraint] {
        let viewList = self.subviews
        if viewList.isEmpty {
            return []
        }

        var fixAll: CGFloat = 0
        var percentAll: CGFloat = 0
        var weightAll: CGFloat = 0
        for v in viewList {
            if let p = v.flexParams {
                fixAll += p.top ?? 0
                fixAll += p.bottom ?? 0
                if let h = p.height {
                    switch h {
                    case .wrap:
                        fixAll += v.fittingSize.height
                    case .fixed(let v):
                        fixAll += v
                    case .percent(let v):
                        percentAll += v
                    case .weight(let v):
                        weightAll += v
                    }
                } else {
                    fixAll += v.fittingSize.height
                }
            } else {
                fixAll += v.fittingSize.height
            }
        }
        fixAll += space * viewList.count - space
        var list: [NSLayoutConstraint] = []
        if weightAll > 0 {
            let heightConstraint: NSLayoutConstraint = heightGuide.heightAnchor.constraint(
                equalTo: contentGuide.heightAnchor, multiplier: 1 - percentAll.limit(from: 0, to: 1), constant: -fixAll
            ).priority(.dragThatCanResizeWindow)
            list += heightConstraint
        }

        var preView: XView? = nil
        for itemView in viewList {
            let cp = itemView.flexParams ?? itemParam
            if cp.huggingPriorityLower {
                itemView.setContentHuggingPriority(.defaultLow, for: .vertical)
                itemView.setContentHuggingPriority(.defaultLow, for: .horizontal)
            }

            // horizontal
            var xCount: Int = 0
            if let left = cp.left {
                list += itemView.leftAnchor.constraint(equalTo: contentGuide.leftAnchor, constant: left)
                xCount += 1
            }
            if let right = cp.right {
                list += itemView.rightAnchor.constraint(equalTo: contentGuide.rightAnchor, constant: -right).priority(.dragThatCanResizeWindow)
                xCount += 1
            }
            if let centerX = cp.centerX, centerX {
                list += itemView.centerXAnchor.constraint(equalTo: contentGuide.centerXAnchor, constant: cp.biasX ?? 0)
                xCount += 1
            }
            if xCount == 0 {
                list += itemView.leftAnchor.constraint(equalTo: contentGuide.leftAnchor, constant: 0)
                xCount = 1
            }
            // vertical
            var yCount: Int = 0
            if let top = cp.top {
                list += itemView.topAnchor.constraint(
                    equalTo: preView?.bottomAnchor ?? contentGuide.topAnchor,
                    constant: top + (preView?.flexParams?.bottom ?? 0) + (preView == nil ? 0 : space))
                yCount += 1
            }
            if let centerY = cp.centerY, centerY {
                list += itemView.centerYAnchor.constraint(equalTo: contentGuide.centerYAnchor, constant: cp.biasY ?? 0)
                yCount += 1
            }
            if yCount == 0 {
                list += itemView.topAnchor.constraint(
                    equalTo: preView?.bottomAnchor ?? contentGuide.topAnchor,
                    constant: 0 + (preView?.flexParams?.bottom ?? 0) + (preView == nil ? 0 : space))
                yCount = 1
            }

            if let width = cp.width {
                switch width {
                case .wrap:
                    itemView.keepContent(.horizontal)
                case .fixed(let value):
                    list += itemView.widthAnchor.constraint(equalToConstant: value)
                case .weight:
                    list += itemView.widthAnchor.constraint(equalTo: contentGuide.widthAnchor, multiplier: 1, constant: -(cp.left ?? 0) - (cp.right ?? 0)).priority(
                        .dragThatCanResizeWindow)
                case .percent(let value):
                    list += itemView.widthAnchor.constraint(equalTo: contentGuide.widthAnchor, multiplier: value).priority(.dragThatCanResizeWindow)
                }
            } else if xCount < 2 {
                itemView.keepContent(.horizontal)
            }
            if let height = cp.height {
                switch height {
                case .wrap:
                    itemView.keepContent(.vertical)
                case .fixed(let value):
                    list += itemView.heightAnchor.constraint(equalToConstant: value)
                case .weight(let value):
                    if weightAll > 0 && value > 0 {
                        list += itemView.heightAnchor.constraint(equalTo: heightGuide.heightAnchor, multiplier: value / weightAll, constant: 0).priority(
                            .dragThatCanResizeWindow)
                    }
                    break  // TODO height
                case .percent(let value):
                    if value > 0 {
                        list += itemView.heightAnchor.constraint(equalTo: contentGuide.heightAnchor, multiplier: value).priority(.dragThatCanResizeWindow)
                    }
                }
            } else if yCount < 2 {
                itemView.keepContent(.vertical)
            }

            preView = itemView
        }
        let lastView = viewList.last!
        if let bottom = lastView.flexParams?.bottom {
            list += lastView.bottomAnchor.constraint(lessThanOrEqualTo: contentGuide.bottomAnchor, constant: -bottom)
        }
        return list
    }
}

public class FlexRow: BaseLayoutView {
    private let widthGuide: XLayoutGuide = XLayoutGuide()
    public var space: CGFloat = 0
    public var itemParam: FlexParam = FlexParam().apply { p in
        p.left = 0
        p.top = 0
        p.width = .wrap
        p.height = .wrap
    }
    public override func onInit() {
        self.addLayoutGuide(widthGuide)
    }
    public override func onBuildConstraints() -> [NSLayoutConstraint] {
        let viewList = self.subviews
        if viewList.isEmpty {
            return []
        }

        var fixAll: CGFloat = 0
        var percentAll: CGFloat = 0
        var weightAll: CGFloat = 0
        for v in viewList {
            if let p = v.flexParams {
                fixAll += p.left ?? 0
                fixAll += p.right ?? 0
                if let w = p.width {
                    switch w {
                    case .wrap:
                        fixAll += v.fittingSize.width
                    case .fixed(let v):
                        fixAll += v
                    case .percent(let v):
                        percentAll += v
                    case .weight(let v):
                        weightAll += v
                    }
                } else {
                    fixAll += v.fittingSize.width
                }
            } else {
                fixAll += v.fittingSize.width
            }
        }
        fixAll += space * viewList.count - space
        var list: [NSLayoutConstraint] = []
        if weightAll > 0 {
            let widthConstraint: NSLayoutConstraint = widthGuide.widthAnchor.constraint(
                equalTo: contentGuide.widthAnchor, multiplier: 1 - percentAll.limit(from: 0, to: 1), constant: -fixAll
            ).priority(.dragThatCanResizeWindow)
            list += widthConstraint
        }

        var preView: XView? = nil
        for itemView in viewList {
            let cp = itemView.flexParams ?? itemParam
            if cp.huggingPriorityLower {
                itemView.setContentHuggingPriority(.defaultLow, for: .vertical)
                itemView.setContentHuggingPriority(.defaultLow, for: .horizontal)
            }

            // x
            var xCount: Int = 0
            if let left = cp.left {
                list += itemView.leftAnchor.constraint(
                    equalTo: preView?.rightAnchor ?? contentGuide.leftAnchor,
                    constant: left + (preView?.flexParams?.right ?? 0) + (preView == nil ? 0 : space))
                xCount += 1
            }

            if let centerX = cp.centerX, centerX {
                list += itemView.centerXAnchor.constraint(equalTo: contentGuide.centerXAnchor, constant: cp.biasX ?? 0)
                xCount += 1
            }
            if xCount == 0 {
                list += itemView.leftAnchor.constraint(
                    equalTo: preView?.rightAnchor ?? contentGuide.leftAnchor,
                    constant: 0 + (preView?.flexParams?.right ?? 0) + (preView == nil ? 0 : space))
                xCount = 1
            }

            // y
            var yCount: Int = 0
            if let top = cp.top {
                list += itemView.topAnchor.constraint(equalTo: contentGuide.topAnchor, constant: top)
                yCount += 1
            }
            if let bottom = cp.bottom {
                list += itemView.bottomAnchor.constraint(equalTo: contentGuide.bottomAnchor, constant: -bottom)
                yCount += 1
            }

            if let centerY = cp.centerY, centerY {
                list += itemView.centerYAnchor.constraint(equalTo: contentGuide.centerYAnchor, constant: cp.biasY ?? 0)
                yCount += 1
            }
            if yCount == 0 {
                list += itemView.topAnchor.constraint(equalTo: contentGuide.topAnchor, constant: 0)
                yCount = 1
            }

            if let width = cp.width {
                switch width {
                case .wrap:
                    itemView.keepContent(.horizontal)
                case .fixed(let value):
                    list += itemView.widthAnchor.constraint(equalToConstant: value)
                case .weight(let value):
                    if weightAll > 0 && value > 0 {
                        list += itemView.widthAnchor.constraint(equalTo: widthGuide.widthAnchor, multiplier: value / weightAll, constant: 0).priority(
                            .dragThatCanResizeWindow)
                    }

                case .percent(let value):
                    if value > 0 {
                        list += itemView.widthAnchor.constraint(equalTo: contentGuide.widthAnchor, multiplier: value).priority(.dragThatCanResizeWindow)
                    }
                }
            } else if xCount < 2 {
                itemView.keepContent(.horizontal)
            }
            if let height = cp.height {
                switch height {
                case .wrap:
                    itemView.keepContent(.vertical)
                case .fixed(let value):
                    list += itemView.heightAnchor.constraint(equalToConstant: value)
                case .weight:
                    list += itemView.heightAnchor.constraint(equalTo: contentGuide.heightAnchor, multiplier: 1, constant: -(cp.top ?? 0) - (cp.bottom ?? 0))
                case .percent(let value):
                    list += itemView.heightAnchor.constraint(equalTo: contentGuide.heightAnchor, multiplier: value)

                }
            } else if yCount < 2 {
                itemView.keepContent(.vertical)
            }

            preView = itemView
        }
        let lastView = viewList.last!
        if let right = lastView.flexParams?.right {
            list += lastView.rightAnchor.constraint(lessThanOrEqualTo: contentGuide.rightAnchor, constant: -right)
        }
        return list
    }
}

open class ColumnPage: HarePage {
    public let columnView: FlexColumn = FlexColumn(frame: NSRect(x: 0, y: 0, width: 800, height: 600))
    open override func loadView() {
        self.view = columnView
        MsgCenter.add(listener: self)
    }
}

open class RowPage: HarePage {
    public let rowView: FlexRow = FlexRow(frame: NSRect(x: 0, y: 0, width: 800, height: 600))
    open override func loadView() {
        self.view = rowView
        MsgCenter.add(listener: self)
    }
}
