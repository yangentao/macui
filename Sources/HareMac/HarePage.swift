//
// Created by entaoyang@163.com on 2022/10/2.
//

import AppKit
import Foundation
import HareSwift

public typealias ToolValidator = () -> Bool
open class HarePage: NSViewController, NSWindowDelegate, ToolbarActionCallback, MenuActionCallback, MsgListener, NSToolbarItemValidation {
    public var toolbarAuto: Bool = true
    public var toolbarItems: [NSToolbarItem] = []
    private var validMap: [Ident: ToolValidator] = [:]

    @MainActor
    public init() {
        super.init(nibName: nil, bundle: nil)
        onInit()
    }
    @MainActor
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    open func onInit() {}

    @objc
    open func windowShouldClose(_ sender: NSWindow) -> Bool {
        return true
    }

    @objc
    open func windowWillClose(_ notification: Notification) {

    }

    open override func loadView() {
        view = NSView(frame: NSRect(x: 0, y: 0, width: 960, height: 640))
        MsgCenter.add(listener: self)

    }

    open func onMsg(msg: Msg) {

    }

    @objc
    open func onToolbarAction(ident: Ident, index: Int) {

    }

    @objc
    open func onMenuAction(item: NSMenuItem) {

    }

    open func onConfig(window: NSWindow) {
    }
    public func toolbar(_ block: (ToolsBuilder) -> Void) {
        let b = ToolsBuilder()
        block(b)
        if b.items.notEmpty {
            installTools(b.items)
            self.toolbarItems.addAll(b.items)
        }
    }
    open override func viewDidAppear() {
        super.viewDidAppear()
        if toolbarAuto {
            window?.addToolItems(toolbarItems)
        }
    }
    open override func viewWillDisappear() {
        if toolbarAuto {
            window?.removeToolItems(toolbarItems)
        }
        super.viewWillDisappear()
    }
    public func validateToolbarItem(_ item: NSToolbarItem) -> Bool {
        let ident = item.itemIdentifier.identUI
        return validMap[ident]?() ?? true
    }

    public func toolValid(_ ident: Ident, _ validator: @escaping ToolValidator) {
        validMap[ident] = validator
    }
    open func close() {
        if nil != self.presentingViewController {
            self.dismiss(nil)
        } else {
            if let w = self.window {
                w.maybeClose()
            }
        }
    }

    open func show(title: String? = nil, level: NSWindow.Level? = nil) {
        NSApp.show(page: self, title: title, level: level)
    }

    open func showModal(title: String? = nil, level: NSWindow.Level? = nil) {
        NSApp.show(modal: self, title: title, level: level)
    }

    open class func showSinglton(title: String? = nil, level: NSWindow.Level? = nil) {
        NSApp.show(singlton: Self.self, title: title, level: level)
    }
}

open class SafePage: HarePage {
    public let safeView: NSView = NSView(frame: .zero)

    open override func loadView() {
        super.loadView()
        self.view.add(view: safeView)
        safeView.anchors { a in
            a.top.equal(anchor: self.view.safeAreaLayoutGuide.topAnchor)
            a.bottom.equal(anchor: self.view.safeAreaLayoutGuide.bottomAnchor)
            a.left.equal(anchor: self.view.safeAreaLayoutGuide.leftAnchor)
            a.right.equal(anchor: self.view.safeAreaLayoutGuide.rightAnchor)
        }
    }
}
