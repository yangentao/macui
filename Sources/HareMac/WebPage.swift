import Foundation
import AppKit
import WebKit
import HareSwift

open class WebPage: HarePage, WKUIDelegate, WKNavigationDelegate {
    public let webView: WKWebView = WKWebView(frame: .zero, configuration: WKWebViewConfiguration())
    public var topMargin: CGFloat = 0
    public var leftMargin: CGFloat = 0
    public var rightMargin: CGFloat = 0
    public var bottomMargin: CGFloat = 0

    public var url: URL? = nil
    public var onLoadCallback: VoidBlock? = nil

    open override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(webView)
        webView.anchors {
            $0.edges(inset: 0)
        }
        webView.navigationDelegate = self
        webView.uiDelegate = self
        if let u = self.url {
            webView.load(URLRequest(url: u))
        }
        onLoadCallback?()
    }

    open func load(url: URL) {
        self.webView.load(URLRequest(url: url))
    }

    open func load(string: String) {
        if let u = URL(string: string) {
            self.webView.load(URLRequest(url: u))
        } else {
            logd("url parse error: ", url)
        }
    }
}


