//
// Created by entaoyang@163.com on 2022/9/30.
//

import Foundation

public class GroupData {
    public let rootURL: URL
    public let groupData: UserDefaults

    public init(group: String) {
        rootURL = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: group)!
        groupData = UserDefaults(suiteName: group)!
    }

    //let url = dir(name:"images")

    public func dir(name: String) -> URL {
        let u: URL = rootURL.appendingPathComponent(name, isDirectory: true)
        if !u.path.existDirectory {
            try? FileManager.default.createDirectory(at: u, withIntermediateDirectories: true)
        }
        return u
    }

    public func get(key: String) -> String? {
        groupData.string(forKey: key)
    }

    public func set(key: String, value: String?) {
        groupData.set(value, forKey: key)
    }
}
