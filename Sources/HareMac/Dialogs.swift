//
// Created by entaoyang@163.com on 2022/9/22.
//

import AppKit
import Foundation
import HareSwift
import UniformTypeIdentifiers


@MainActor
public func OpenPanelSingleDirectory(url: URL?, title: String? = nil, message: String? = nil, prompt: String? = nil, level: NSWindow.Level? = nil) async
    -> URL?
{
    return await OpenPanel(dir: url, multi: false, dirs: true, files: false, title: title, message: message, prompt: prompt, level: level).first
}
@MainActor
public func OpenPanelSingleFile(url: URL?, title: String? = nil, message: String? = nil, prompt: String? = nil, types: [UTType] = [], level: NSWindow.Level? = nil) async
    -> URL?
{
    return await OpenPanel(dir: url, multi: false, dirs: false, files: true, title: title, message: message, prompt: prompt, types: types, level: level).first
}
@MainActor
public func OpenPanel(
    dir: URL?, multi: Bool = false, dirs: Bool = true, files: Bool = true, title: String? = nil, message: String? = nil, prompt: String? = nil, types: [UTType] = [],
    level: NSWindow.Level? = nil
) async
    -> [URL]
{
    let dlg = NSOpenPanel()
    dlg.config(dir: dir, title: title, message: message, prompt: prompt, types: types, level: level)

    dlg.allowsMultipleSelection = multi
    dlg.canChooseDirectories = dirs
    dlg.canChooseFiles = files

    let r = await dlg.begin()
    if r == NSApplication.ModalResponse.OK {
        return dlg.urls
    }
    return []
}
@MainActor
public func SavePanel(
    dir: URL?, filename: String? = nil, title: String? = nil, message: String? = nil, prompt: String? = nil, types: [UTType] = [], level: NSWindow.Level? = nil
) async
    -> URL?
{
    let dlg = NSSavePanel()
    dlg.config(dir: dir, title: title, message: message, prompt: prompt, types: types, level: level)
    if let fn = filename {
        dlg.nameFieldStringValue = fn
    }

    let r = await dlg.begin()
    if r == NSApplication.ModalResponse.OK {
        return dlg.url
    }
    return nil
}
extension NSSavePanel {
    fileprivate func config(dir: URL?, title: String? = nil, message: String? = nil, prompt: String? = nil, types: [UTType] = [], level: NSWindow.Level? = nil) {
        let dlg = self
        if let u = dir {
            dlg.directoryURL = u.existFile ? u.deletingLastPathComponent() : u
        } else {
            dlg.directoryURL = RealHomeDirectory()
        }

        if let t = title {
            dlg.title = t
        }
        if let msg = message {
            dlg.message = msg
        }

        if let p = prompt {
            dlg.prompt = p
        }
        if let lv = level {
            dlg.level = lv
        }
        if types.notEmpty {
            dlg.allowedContentTypes = types
        }
    }
}

public func confirmAlert(msg: String, info: String = "", ok: String = "ok".local) -> Bool {
    let a = NSAlert()
    a.messageText = msg
    a.informativeText = info
    a.alertStyle = .warning
    a.icon = "exclamationmark.triangle".symbol?.config(color: .systemYellow)
    a.addButton(withTitle: ok)
    a.addButton(withTitle: "cancel".local)
    return a.runModal() == NSApplication.ModalResponse.alertFirstButtonReturn
}

public func alert(info msg: String, text: String = "", ok: String = "ok".local) {
    let a = NSAlert()
    a.messageText = msg
    a.informativeText = text
    a.alertStyle = .informational
    a.icon = "info.circle".symbol?.config(color: .textColor)
    a.addButton(withTitle: ok)
    a.runModal()
}

public func alert(warn msg: String, text: String = "", ok: String = "ok".local) {
    let a = NSAlert()
    a.messageText = msg
    a.informativeText = text
    a.alertStyle = .warning
    a.icon = "exclamationmark.triangle".symbol?.config(color: .systemYellow)
    a.addButton(withTitle: ok)
    a.runModal()
}

public func alert(error msg: String, text: String = "", ok: String = "ok".local) {
    let a = NSAlert()
    a.messageText = msg
    a.informativeText = text
    a.alertStyle = .critical
    a.addButton(withTitle: ok)
    a.runModal()
}

extension NSSavePanel {

    public func showModal() -> [URL] {
        if self.runModal() == NSApplication.ModalResponse.OK {
            return self.resultURLs
        }
        return []
    }

    public var resultURLs: [URL] {
        if let op = self as? NSOpenPanel {
            if !op.urls.isEmpty {
                return op.urls
            }
        } else {
            if let u = self.url {
                return [u]
            }
        }
        return []
    }

    public func showModalSheet(_ window: NSWindow, _ block: @escaping ([URL]) -> Void) {
        self.beginSheetModal(for: window) { r in
            if r == NSApplication.ModalResponse.OK {
                let us = self.resultURLs
                block(us)
            }
        }
    }

    //回调 [URL] 非空

    public func showModeless(_ block: @escaping ([URL]) -> Void) {
        self.begin { r in
            if r == NSApplication.ModalResponse.OK {
                let us = self.resultURLs
                block(us)
            }
        }
    }
}
