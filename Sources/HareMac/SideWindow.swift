//
// Created by entaoyang@163.com on 2022/11/6.
//

import AppKit
import Foundation
import HareSwift

class SplitPage: NSSplitViewController {
    open override func loadView() {
        view = NSView(frame: NSRect(x: 0, y: 0, width: 800, height: 600))
    }
}

open class SideWindow: NSObject, NSWindowDelegate, MsgListener, ToolbarActionCallback {

    public let splitPage: NSSplitViewController
    public let sidePage: SidePage
    public let contentPage: SideContentPage
    public let leftItem: NSSplitViewItem
    public let contentItem: NSSplitViewItem
    public let window: NSWindow
    private let leftToggle: LeftTogglePage

    public init(ident: Ident) {
        splitPage = NSSplitViewController(nibName: nil, bundle: nil)
        sidePage = SidePage()
        contentPage = SideContentPage()
        splitPage.splitView.isVertical = true
        splitPage.splitView.dividerStyle = .paneSplitter
        leftItem = NSSplitViewItem(sidebarWithViewController: sidePage).apply { $0.titlebarSeparatorStyle = .none }
        contentItem = NSSplitViewItem(contentListWithViewController: contentPage).apply { $0.titlebarSeparatorStyle = .none }
        splitPage.addSplitViewItem(leftItem)
        splitPage.addSplitViewItem(contentItem)
        leftToggle = LeftTogglePage(nibName: nil, bundle: nil)
        window = HareWindow(page: splitPage)
        window.identifier = ident
        super.init()
        window.delegate = self

        sidePage.onSelectItem = { [weak self] item in
            self?.showItemView(item: item)
        }
        canCollapseSidebar = true
        onBuildWindow()
        reload()
        window.setContentSize(.init(width: 960, height: 640))
        MsgCenter.add(listener: self)
        afterCreate()
    }

    open func onMsg(msg: Msg) {

    }
    open func afterCreate() {}

    open func onBuildWindow() {

    }
    open func onToolbarAction(ident: Ident, index: Int) {

    }
    private func showItemView(item: SideLeaf) {
        contentPage.set(page: item.page)
    }

    public var canCollapseSidebar: Bool {
        get {
            leftItem.canCollapse
        }
        set {
            leftItem.canCollapse = newValue
            installLeftToggle(newValue)
        }
    }

    private func installLeftToggle(_ on: Bool) {
        let ls = window.titlebarAccessoryViewControllers
        if on {
            leftToggle.toggleCallback = { [unowned self] in
                self.splitPage.toggleSidebar(nil)
            }
            for p in ls {
                if p === leftToggle {
                    return
                }
            }
            window.addTitlebarAccessoryViewController(leftToggle)
        } else {

            for idx in ls.indices {
                if ls[idx] === leftToggle {
                    window.removeTitlebarAccessoryViewController(at: idx)
                    return
                }
            }
        }
    }

    public func toolbar(_ block: (ToolsBuilder) -> Void) {
        let ls = ToolsBuilder().apply(block).items
        if ls.notEmpty {
            installTools(ls)
            window.addToolItems(ls)
        }
    }

    public func show() {
        for w in NSApp.windows {
            if w.identifier != nil && w.identifier == window.identifier {
                w.orderFront(nil)
                return
            }
        }
        window.makeKeyAndOrderFront(nil)
    }
}

extension SideWindow {
    @discardableResult
    public func group(title: String, _ block: (SideGroup) -> Void) -> SideGroup {
        self.sidePage.addGroup(title: title, block)
    }

    public func reload() {
        self.sidePage.reload()
    }
}

public class SideContentPage: HarePage {
    public override func viewDidLoad() {
        super.viewDidLoad()

    }

    public func set(page: NSViewController) {
        let vlist = self.view.subviews
        for v in vlist {
            v.removeFromSuperview()
        }
        let plist = self.children
        for c in plist {
            c.removeFromParent()
        }
        self.addChild(page)
        self.view.add(view: page.view).anchors { a in
            a.left(0)
            a.top(0)
            a.width.equal(view: a.parent)
            a.height.equal(view: a.parent)
        }
    }

}

private class LeftTogglePage: NSTitlebarAccessoryViewController {

    var toggleCallback: () -> Void = {
    }

    override func loadView() {
        self.view = NSView(frame: NSRect(width: 64, height: 32))
        self.layoutAttribute = .left
        let b = NSButton(image: "rectangle.leftthird.inset.filled".symbol!.config(size: 24), target: self, action: #selector(onButtonClick(_:)))
        b.bezelStyle = .texturedRounded
        self.view.add(view: b)
        b.anchors { a in
            a.center()
            a.size(24)
        }
    }

    @objc
    func onButtonClick(_ b: NSButton) {
        logd("click ")
        toggleCallback()
    }

}
