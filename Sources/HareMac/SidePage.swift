//
// Created by entaoyang@163.com on 2022/11/5.
//

import AppKit
import Foundation
import HareSwift

//topView.updateHeight(150 )
open class SidePage: HarePage, NSOutlineViewDataSource, NSOutlineViewDelegate {
    //    public let topView = NSView(frame: .zero)
    public let scrollView = NSScrollView(frame: .zero)
    public let outlineView = SideOutlineView(frame: .zero)
    public private(set) var groups: [SideGroup] = []
    public var onSelectItem: (SideLeaf) -> Void = { _ in
    }

    open override func viewDidLoad() {
        super.viewDidLoad()
        self.view.frame = NSRect(width: 200, height: 500)
        self.view.view(scrollView) { v in
            v.anchors { a in
                a.edgeX(inset: 0)
                a.top(0)
                a.bottom(0)
            }
            v.hasHorizontalScroller = false
            v.hasVerticalScroller = true
            v.autohidesScrollers = true
            v.documentView = outlineView
        }
        outlineView.style = .sourceList
        outlineView.rowSizeStyle = .large
        outlineView.gridStyleMask = []
        outlineView.indentationPerLevel = 0
        outlineView.intercellSpacing = NSSize(width: 0, height: 4)
        outlineView.allowsMultipleSelection = false
        outlineView.column(ident: Ident("first"), title: "Title", width: 200, resizing: [.autoresizingMask], sortable: false)
        outlineView.headerView = nil
        outlineView.outlineTableColumn?.width = 0
        outlineView.dataSource = self
        outlineView.delegate = self

    }

    public func setItems(_ items: [SideGroup]) {
        self.groups = items
        self.reload()
    }

    @discardableResult
    public func addGroup(title: String, _ block: (SideGroup) -> Void) -> SideGroup {
        let g = SideGroup(title: title)
        self.groups.add(g)
        block(g)
        return g
    }

    @discardableResult
    public func add(group: SideGroup, _ block: (SideGroup) -> Void) -> SideGroup {
        self.groups.add(group)
        block(group)
        return group
    }

    public func reload() {
        let idx = self.outlineView.selectedRow
        self.outlineView.reloadData()
        self.outlineView.expandItem(nil, expandChildren: true)
        let newCount = self.outlineView.numberOfRows
        if idx < 0 {
            if newCount > 1 {
                self.outlineView.selectRowIndexes(IndexSet(integer: 1), byExtendingSelection: false)
            }
        } else if idx < newCount {
            self.outlineView.selectRowIndexes(IndexSet(integer: idx), byExtendingSelection: false)
        }
    }

    @objc
    open func outlineViewSelectionDidChange(_ notification: Notification) {
        let idx = outlineView.selectedRow
        if idx >= 0 {
            if let a = outlineView.item(atRow: idx), let item = a as? SideLeaf {
                onSelectItem(item)
            }
        }
    }

    @objc
    open func outlineView(_ outlineView: NSOutlineView, viewFor tableColumn: NSTableColumn?, item: Any) -> NSView? {
        if let g = item as? SideGroup {
            let v =
                outlineView.makeView(withIdentifier: g.viewtype, owner: nil)
                ?? g.creator().apply {
                    $0.identifier = g.viewtype
                }
            g.binder(g, v)
            return v
        } else if let a = item as? SideLeaf {
            let v =
                outlineView.makeView(withIdentifier: a.viewtype, owner: nil)
                ?? a.creator().apply {
                    $0.identifier = a.viewtype
                }
            a.binder(a, v)
            return v
        }
        return nil

    }

    @objc
    open func outlineView(_ outlineView: NSOutlineView, heightOfRowByItem item: Any) -> CGFloat {
        if let g = item as? SideGroup, g.title.isEmpty {
            return 1
        }
        if let g = item as? SideItem {
            return g.height
        }
        return 32
    }

    @objc
    open func outlineView(_ outlineView: NSOutlineView, shouldSelectItem item: Any) -> Bool {
        !(item is SideGroup)
    }

    @objc
    open func outlineView(_ outlineView: NSOutlineView, shouldCollapseItem item: Any) -> Bool {
        false
    }

    @objc
    public func outlineView(_ outlineView: NSOutlineView, shouldShowOutlineCellForItem item: Any) -> Bool {
        false
    }

    @objc
    open func outlineView(_ outlineView: NSOutlineView, numberOfChildrenOfItem item: Any?) -> Int {
        if item == nil {
            return groups.count
        }
        if let node = item as? SideGroup {
            return node.children.count
        }
        return 0
    }

    @objc
    open func outlineView(_ outlineView: NSOutlineView, child index: Int, ofItem item: Any?) -> Any {
        if item == nil {
            return groups[index]
        }
        if let node = item as? SideGroup {
            return node.children[index]
        }
        return 0
    }

    @objc
    open func outlineView(_ outlineView: NSOutlineView, isItemExpandable item: Any) -> Bool {
        if let node = item as? SideGroup {
            return node.children.notEmpty
        }
        return false
    }
}

public class SideOutlineView: NSOutlineView {
    public override init(frame: NSRect) {
        super.init(frame: frame)
    }

    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public override func frameOfOutlineCell(atRow row: Int) -> NSRect {
        return .zero
    }

    public override func frameOfCell(atColumn column: Int, row: Int) -> NSRect {
        let r = super.frameOfCell(atColumn: column, row: row)
        //        if column == 0 {
        //            return NSRect(x: 0, y: r.origin.y, width: r.size.width, height: r.size.height)
        //        }
        return r
    }
}

public class SideLeafView: NSView {
    public let imageView: NSImageView = NSImageView(frame: .zero)
    public let titleView: Label = Label(frame: .zero)

    public override init(frame: NSRect) {
        super.init(frame: frame)
        self.add(view: imageView)
        self.add(view: titleView)
        imageView.anchors { a in
            a.size(24)
            a.left(0)
            a.centerY()
        }
        titleView.anchors { a in
            a.edgeY(inset: 0)
            a.toRightPre(8)
            a.right(0)
        }
        titleView.font = .systemFont(ofSize: 13, weight: .regular)
        titleView.alignVertical = .center
        imageView.imageScaling = .scaleProportionallyUpOrDown
    }

    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public var image: NSImage? {
        get {
            imageView.image
        }
        set {
            imageView.image = newValue
        }
    }
    public var title: String {
        get {
            titleView.stringValue
        }
        set {
            titleView.stringValue = newValue
        }
    }

}

public class SideGroupView: Label {
    public override init(frame: NSRect) {
        super.init(frame: frame)
        self.font = .systemFont(ofSize: 11, weight: .regular)
        self.textColor = .secondaryLabelColor
        self.alignVertical = .end
    }

    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public var title: String {
        get {
            self.stringValue
        }
        set {
            self.stringValue = newValue
        }
    }
}

public class SideItem: NSObject {
    public let title: String
    public var value: Any? = nil
    public var height: CGFloat = 32

    public var viewtype: Ident = Ident(Label.self.className())

    public var creator: () -> NSView = {
        Label(frame: .zero)
    }

    public init(title: String) {
        self.title = title
        super.init()
    }

}

public class SideLeaf: SideItem {
    public var cachePage: Bool = true
    private var pageCached: NSViewController? = nil
    public let image: NSImage?
    public var binder: (SideLeaf, NSView) -> Void = { item, view in
        if let lb = view as? NSTextField {
            lb.stringValue = item.title
        }
    }
    private var contentCreator: () -> NSViewController = {
        SafePage()
    }

    public init(title: String, image: NSImage?) {
        self.image = image
        super.init(title: title)
        self.height = 32
        self.viewtype = Ident(SideLeafView.self.className())
        self.creator = {
            SideLeafView(frame: .zero)
        }
        self.binder = { item, view in
            if let v = view as? SideLeafView {
                v.title = item.title
                v.image = item.image
            }
        }
    }
    public var page: NSViewController {
        if let p = pageCached { return p }
        let p = contentCreator()
        self.pageCached = p
        return p
    }

    public func contentView(_ block: @escaping () -> NSViewController) {
        self.contentCreator = block
    }

    @discardableResult
    public func contentView<T: NSViewController>(type: T.Type) -> SideLeaf {
        contentCreator = {
            type.init()
        }
        return self
    }

    public func custom<T: NSView>(height: CGFloat, type: T.Type, _ binder: @escaping (SideLeaf, T) -> Void) {
        self.height = height
        viewtype = Ident(type.className())
        self.binder = { item, v in
            binder(item, v as! T)
        }
        self.creator = {
            type.init(frame: .zero)
        }
    }
}

public class SideGroup: SideItem {
    public var children: [SideLeaf]
    public var binder: (SideGroup, NSView) -> Void = { item, view in
        if let lb = view as? SideGroupView {
            lb.title = item.title
        }
    }

    public init(title: String, children: [SideLeaf] = []) {
        self.children = children
        super.init(title: title)
        self.height = 24
        self.viewtype = Ident(SideGroupView.self.className())
        self.creator = {
            SideGroupView(frame: .zero)
        }
        self.binder = { item, view in
            if let v = view as? SideGroupView {
                v.title = item.title
            }
        }
    }

    public func view<T: NSView>(type: T.Type, _ binder: @escaping (SideGroup, T) -> Void) {
        viewtype = Ident(type.className())
        self.binder = { item, v in
            binder(item, v as! T)

        }
        self.creator = {
            type.init(frame: .zero)
        }
    }

    public subscript(_ index: Int) -> SideLeaf {
        children[index]
    }
    public var isEmpty: Bool {
        return children.isEmpty
    }

    @discardableResult
    public func item(item: SideLeaf) -> SideLeaf {
        children.add(item)
        return item
    }

    @discardableResult
    public func item(title: String, image: NSImage?) -> SideLeaf {
        let a = SideLeaf(title: title, image: image)
        children.add(a)
        return a
    }

    @discardableResult
    public func item<V: NSViewController>(title: String, image: NSImage?, type: V.Type) -> SideLeaf {
        let a = SideLeaf(title: title, image: image)
        a.contentView(type: type)
        children.add(a)
        return a
    }

    @discardableResult
    public func item(title: String, image: NSImage?, _ block: @escaping () -> NSViewController) -> SideLeaf {
        let a = SideLeaf(title: title, image: image)
        a.contentView(block)
        children.add(a)
        return a
    }
}
