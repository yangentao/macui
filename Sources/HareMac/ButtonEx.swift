//
// Created by yangentao on 2019/10/23.
// Copyright (c) 2019 yangentao. All rights reserved.
//

import Foundation
import Cocoa
import HareSwift

public func CheckBox(title:String, target:Any? = nil, action:Selector? = nil )->NSButton{
    return NSButton(checkboxWithTitle: title, target: target, action: action)
}
public func RadioBox(title:String, target:Any? = nil, action:Selector? = nil )->NSButton{
    return NSButton(radioButtonWithTitle: title, target: target, action: action)
}
public func Button(title:String, target:Any? = nil, action:Selector? = nil )->NSButton{
    return NSButton(title: title, target: target, action: action)
}

public extension NSButton {
    convenience init(title: String) {
        self.init(title: title, target: nil, action: nil)
    }

    convenience init(image: String) {
        self.init(image: NSImage(named: image)!, target: nil, action: nil)
    }
}

fileprivate extension AttrKey {
    static let click_time: AttrKey = .init("_click_time")
}

public class ActionControl: NSControl {
    private var _clickTime: TimeInterval? {
        get {
            self[.click_time]
        }
        set {
            self[.click_time] = newValue
        }
    }

    private func invokeAction() {
        if let ac = self.action {
            NSApp.sendAction(ac, to: self.target, from: self)
            return
        }
    }

    open override func mouseUp(with event: NSEvent) {
        if event.type == NSEvent.EventType.leftMouseUp {
            if event.clickCount == 1 {
                _clickTime = Date().timeIntervalSince1970
                Tasks.fore {
                    self.invokeAction()
                }
            } else if (event.clickCount > 1) {
                let t = _clickTime ?? 0.0
                let cur = Date().timeIntervalSince1970
                if cur - t > 0.5 {
                    _clickTime = cur
                    Tasks.fore {
                        self.invokeAction()
                    }
                }
            }
        }
        super.mouseUp(with: event)
    }
}

public extension NSButton {

    func keyReturn() {
        self.keyEquivalent = "\r"
    }

    func keyEscape() {
        self.keyEquivalent = "\u{1B}"
    }
}

public class PopButtonDelegate: NSObject {
    @objc
    public func onItemSelectChanged(_ b: NSPopUpButton) {
        b.title = b.selectedItem?.title ?? ""
    }

    public static var inst: PopButtonDelegate = PopButtonDelegate()
}

public func MakePopButton() -> NSPopUpButton {
    let a = NSPopUpButton(frame: .zero, pullsDown: false)
    a.target = PopButtonDelegate.inst
    a.action = #selector(PopButtonDelegate.onItemSelectChanged(_:))
    return a
}

public func PopDown(frame: NSRect, target: AnyObject?, action: Selector?) -> NSPopUpButton {
    let a = NSPopUpButton(frame: .zero, pullsDown: false)
    a.target = target
    a.action = action
    return a
}

