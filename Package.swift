// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "HareMac",
    defaultLocalization: "en",
    platforms: [
        .macOS(.v12)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "HareMac",
            targets: ["HareMac"]),
    ],
    dependencies: [
        .package(url:"https://gitlab.com/yangentao/hareswift.git",branch: "main")
//        .package(url:"../HareSwift",branch: "main")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "HareMac",
            dependencies: [.product(name:"HareSwift", package:"HareSwift")],
            resources: [.process("Resources")]
        ),
        .testTarget(
            name: "HareMacTests",
            dependencies: ["HareMac"]
        ),
    ],
    swiftLanguageVersions: [.v5]
)
